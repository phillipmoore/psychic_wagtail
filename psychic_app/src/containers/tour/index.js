import axios from 'axios'
import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import Autocomplete from 'react-autocomplete'

import { apiUrl } from '../../utils'

import { setLoading } from '../../modules/counter'

import './tour.css'

class Tours extends React.Component {
  constructor(props) {
    super(props)
    this.iFrameRef = React.createRef()
    this.inputRef = React.createRef()
    this.state = {
      artists: [],
      artistIndex: undefined,
      selectedArtist: "",
      selectedHandle: "",
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSelect = this.handleSelect.bind(this)
    this.resizeIframe = this.resizeIframe.bind(this)
  }

  handleChange (e, item) {
    this.setState({ selectedHandle: e.target.value })
  }

  handleSelect (selectedHandle, item) {
    this.setState({ selectedArtist: item.title })
    this.setState({ selectedHandle: item.bandsintown })
    const selected = this.state.artists.findIndex(artist => artist.id === item.id) 
    this.setState({ artistIndex: selected })
  }

  resizeIframe(obj) {
    obj.target.style.height = obj.target.contentWindow.document.documentElement.scrollHeight + 'px';
  }
  
  componentDidMount () {
    // prevent the keyboard
    document.ontouchmove = function(e){
      e.preventDefault()
    }

    this.inputRef.focus()
    window.scrollTo(0, 0);
    document.body.scrollTop = 0;

    // this.inputRef.current.addEventListener("click", function() {.onFocus = () => {
    //   window.scrollTo(0, 0);
    //   document.body.scrollTop = 0;
    // };

    axios.get(`${apiUrl}/pages/?type=home.ArtistPage&fields=_,id,title,images,bandsintown`).then(res => {
      const touringArtists = res.data.items.filter( artist => { return !!artist.bandsintown })
      this.setState({ artists: touringArtists })
      this.props.setLoading(false)
    })
    
  }

  render () {
    return (
      <div className="tour-container">
        <h1>Tour:
          <Autocomplete
            placeholder="Select Artist"
            getItemValue={(item) => item.title}
            items={this.state.artists}
            renderItem={(item, isHighlighted) =>
              <div 
                key={item.id} 
                style={{ background: isHighlighted ? 'lightgray' : 'white' }}
                data-bit={item.bandsintown}
               >
                {item.title}
              </div>
            }
            value={this.state.selectedArtist}
            onChange={(e, item) => this.handleChange(e, item)}
            onSelect={(selectedHandle, x) => this.handleSelect(selectedHandle, x)}
            ref={el => this.inputRef = el}
            className="auto-complete"
            inputProps = {{readOnly: "readOnly"}}
          />
        </h1>
        <div 
          className="tour-results"
          onClick={this.resizeIframe}
        >
          {!!this.state.selectedHandle &&
            <iframe 
              src={`/assets/bit.html?band=${encodeURIComponent(this.state.selectedHandle)}`}
              onLoad={this.resizeIframe}
              title="artist_tours"
              ref={el => this.iFrameRef = el}
            ></iframe>
          }
          <div className="artist-picture">
            {/*{ ( !!this.state.selectedArtist && this.state.artistIndex !== undefined && this.state.artists.length > 0 && this.state.artists[this.state.artistIndex].images.length > 0) ?
              <img src={this.state.artists[this.state.artistIndex].images[0].value} alt={this.state.artists[this.state.artistIndex].title} /> :
              
            }*/}
            {/*<div className="psychic-eye-image"></div>*/}
          </div>
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLoading
    },
    dispatch
  )

export default connect(
  null,
  mapDispatchToProps
)(Tours)

