import axios from 'axios'
import React from 'react'
import { Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import TinySlider from "tiny-slider-react"

import {
  HiOutlineChevronDoubleLeft,
  HiOutlineChevronDoubleRight
} from 'react-icons/hi'

import { apiUrl, mainUrl } from '../../utils'

import { setLoading } from '../../modules/counter'

import ReleaseListLink from '../releases/releaseListLink'
import { ArtistTitleSplit } from '../releases/releaseListLink'


import './home.css'


// const blankReleaseItem = {
//   title: "",
//   release_image: {
//     url: ""
//   }
// }

const homeGallerySliderSettings = {
  lazyload: true,
  nav: false,
  mouseDrag: true,
  controls: false,
  autoplayButtonOutput: false,
  autoplayHoverPause: false,
  preventActionWhenRunning: true,
  mode: 'gallery',
  autoplay: true,
  speed: 800,
  // animateIn: 'fadeIn',
  // animateOut: 'fadeOut',
  autoplayTimeout: 6000,
  autoHeight: true,
  item: 1
}

const releaseSliderSettings = {
  lazyload: true,
  nav: false,
  mouseDrag: true,
  controls: false,
  responsive: {
    1080: {
      items: 4
    },
    800: {
      items: 2
    },
    600: {
      items: 1
    },
  },
}

// const videoSliderSettings = {
//   // lazyload: true,
//   nav: false,
//   mouseDrag: true,
//   controls: false,
//   rewind: true,
//   responsive: {
//     1080: {
//       items: 3
//     },
//     800: {
//       items: 2
//     },
//     600: {
//       items: 1
//     },
//   },
// }

class Home extends React.Component {
  constructor(props) {
    super(props)
    this.gallerySliderRef = React.createRef()
    this.releaseSliderRef = React.createRef()
    this.videoSliderRef = React.createRef()
    this.state = {
      releaseLinks: [],
      videoLinks: [],
      journalLinks: [],
      foreword: "",
      homeImageBlocks: [],
      galleryTimeout: setTimeout(()=>{})
    }
    this.onGoTo = this.onGoTo.bind(this)
    this.resetAutoplay = this.resetAutoplay.bind(this)
    this.bindTransitionOn = this.bindTransitionOn.bind(this)
    this.bindTransitionOff = this.bindTransitionOff.bind(this)
  }

  componentDidMount () { 
    axios.get(`${apiUrl}/pages/?type=home.homePage&fields=_,id,title,about_text,images`)
    .then(res => {
      if (res.data.items.length > 0) {
        this.setState({homeImageBlocks: res.data.items[0].images})
      }
    })

    // releases
    axios.get(`${apiUrl}/pages/?type=home.ReleasePage&fields=_,id,title,slug,artist,alt_artists,release_image&remove_listing=false&display_on_home_page=true`)
    .then(res => {
      // const itemLength = res.data.items.length
      // const releaseItems = itemLength >= 12 ? res.data.items : res.data.items.concat(new Array(12 - itemLength).fill(blankReleaseItem))
      this.setState({releaseLinks: res.data.items})
      // journals
      axios.get(`${apiUrl}/pages/?type=home.JournalPage&fields=_,id,title,slug,categories,thumbnail_image,abstract&display_on_home_page=true&order=-first_published_at`)
      .then(res => {
        this.setState({journalLinks: res.data.items})
        // videos
        axios.get(`${apiUrl}/pages/?type=home.VideoPage&fields=_,id,title,slug,artist,alt_artists,video_image,embed_code&display_on_home_page=true`)
        .then(res => {
          this.setState({videoLinks: res.data.items})
          this.props.setLoading(false)
        })
      })
    })
  }

  bindTransitionOn (e) {
    e.slideItems[e.indexCached].style.transform = 'scale(1.05,1.05)'
    e.slideItems[e.index].firstChild.style.transform = 'scale(1,1)'

  }

  bindTransitionOff (e) {
    e.slideItems[e.indexCached].style.transform = 'scale(1,1)'
    e.slideItems[e.indexCached].firstChild.style.transform = 'scale(1.1,1.1)'
  }

  resetAutoplay (ref) {
    clearTimeout(this.state.galleryTimeout)
    this.[ref].slider.pause()
    const galleryTimeout = setTimeout(()=>{
      this.[ref].slider.play()
    }, 100)
    this.setState({galleryTimeout: galleryTimeout})
  }

  onGoTo = (dir, ref) => {
    this.[ref].slider.goTo(dir)
    // restart autoplay to prevent glitchy gallery
    if (ref === 'gallerySliderRef') {
      this.resetAutoplay(ref)
    }
  }

  render () {
    return (
      <div className="home-container">
        {/*<h1>Psychic Hotline</h1>*/}
        {this.state.homeImageBlocks.length > 0 && //this.state.homeImageBlocks.length > 0
          <section className="home-top">
            <div className="home-gallery">
              <TinySlider 
                settings={homeGallerySliderSettings}
                ref={gallerySliderRef => this.gallerySliderRef = gallerySliderRef}
                onTransitionStart={this.bindTransitionOn}
                onTransitionEnd={this.bindTransitionOff}
              >
                {this.state.homeImageBlocks.map( (imageBlock, index) => (
                  <div 
                    className="home-gallery-image" 
                    key={`home_image_${index}`}
                    style={{
                      backgroundImage: `url(${imageBlock.value.image.src})`,
                      backgroundPosition: `${imageBlock.value.image.left_split_point}% ${imageBlock.value.image.top_split_point}%`,
                    }}
                  >
                    <div 
                      className={imageBlock.value.title_background ? 
                        "home-gallery-image-content has-background" : "home-gallery-image-content"
                      }
                    > 
                      {imageBlock.value.release &&
                        <div className="title-container">
                          <h2><ArtistTitleSplit title={imageBlock.value.release.artist_title} /></h2>
                          <h3 className="home-gallery-release-title"><span>{imageBlock.value.release.release_title}</span></h3>
                        </div>
                      }
                      <div className="home-gallery-button-wrap">
                        <button 
                          className="home-arrow-prev"
                        >
                          <HiOutlineChevronDoubleLeft />
                        </button>
                        <button 
                          className="home-arrow-next"
                        >
                          <HiOutlineChevronDoubleRight />
                        </button>
                      </div>
                    </div>
                    <div className={imageBlock.value.title_background ? 
                        "home-gallery-image-content cta has-background" : "home-gallery-image-content cta"
                      }
                    >
                      {imageBlock.value.release && 
                        <div>
                          <div className="home-gallery-link-text">
                            <Link className="" to={`/releases/${imageBlock.value.release.slug}`}>
                              {imageBlock.value.release.link_text}
                            </Link>
                          </div>
                        </div>
                      }
                    </div>
                  </div>
                ))}
              </TinySlider>
              <div className="home-gallery-button-wrap active">
                <button 
                  className="home-arrow-prev"
                  type="button" onClick={() => this.onGoTo('prev', 'gallerySliderRef')}
                >
                  <HiOutlineChevronDoubleLeft />
                </button>
                <button 
                  className="home-arrow-next"
                  type="button" onClick={() =>  this.onGoTo('next', 'gallerySliderRef')}
                >
                  <HiOutlineChevronDoubleRight />
                </button>
              </div>
            </div>
          </section>
        }
        <section>
          <h3>Releases</h3>
          <div className="home-product-slides">
            <TinySlider 
              settings={releaseSliderSettings}
              ref={releaseSliderRef => this.releaseSliderRef = releaseSliderRef}
            >
              {this.state.releaseLinks.map( (product, index) => (
                <div className="home-product-image" key={`product_${index}`}>
                  <ReleaseListLink product={product}/>
                </div>
              ))}
          </TinySlider>
          </div>
          <div className="home-button-wrap">
            <button 
              className="home-arrow-prev"
              type="button" onClick={() => this.onGoTo('prev', 'releaseSliderRef')}
            >
              <HiOutlineChevronDoubleLeft />
            </button>
            <button 
              className="home-arrow-next"
              type="button" onClick={() =>  this.onGoTo('next', 'releaseSliderRef')}
            >
              <HiOutlineChevronDoubleRight />
            </button>
          </div>
        </section>
        <section>
        <h3>Journal</h3>
        <div className="home-joural-links">
          {this.state.journalLinks.map( (entry, index) => (
            <div className="home-journal-entry" key={`entry_${index}`}>
              <Link to={`/journal/${entry.meta.slug}`}>
                <div className="home-journal-image">
                  <img src={`${mainUrl}${entry.thumbnail_image.url}`} alt={entry.title} />
                </div>
                <div className="home-journal-text">
                  <h4>{entry.title}</h4>
                  <p>{entry.abstract}</p>
                </div>
              </Link>
            </div>
          ))}
        </div>
        </section>
        {/*<section>
        <h3>Videos</h3>
        <div className="home-product-slides">
          <TinySlider 
            settings={videoSliderSettings}
            ref={videoSliderRef => this.videoSliderRef = videoSliderRef}
          >
            {this.state.videoLinks.map( (video, index) => (
              <div className="home-product-image" key={`video_${index}`}>
                <Video video={video} showAndPlayVideo={this.showAndPlayVideo} />
              </div>
            ))}
        </TinySlider>
        </div>
        <div className="home-button-wrap">
          <button 
            className="home-arrow-prev"
            type="button" onClick={() => this.onGoTo('prev', 'videoSliderRef')}
          >
            <HiOutlineChevronDoubleLeft />
          </button>
          <button 
            className="home-arrow-next"
            type="button" onClick={() =>  this.onGoTo('next', 'videoSliderRef')}
          >
            <HiOutlineChevronDoubleRight />
          </button>
        </div>
        </section>*/}
      </div>
    )
  }
}

const mapStateToProps = ({ counter }) => ({
  releaseUrl: counter.releaseUrl
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLoading
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
