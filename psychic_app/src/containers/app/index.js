import React, { useState, useEffect } from 'react'
import { Route, Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { withRouter } from "react-router";

import Home from '../home'
// import Artists from '../artists'
import ArtistDetail from '../artists/artistDetail'
import Journal from '../journal'
import CategoryList from '../categories/categoryList'
import JournalAuthorList from '../journal/authorList'
import JournalDetail from '../journal/journalDetail'
// import SinglesSeries from '../singlesSeries'
import Releases from '../releases'
import Cart from '../cart'
import CartNav from '../cart/cartNav'
import Tour from '../tour'
import ReleaseDetail from '../releases/releaseDetail'
import Footer from '../footer'

import { IoCloseOutline, IoMenuOutline } from 'react-icons/io5'

import { setLoading } from '../../modules/counter'

import VideoIframeContainer from '../video/videoIFrame'

const Loader = () => {
  return (
    <div className="loader">
      <div className="loader-inner ball-scale-multiple">
        <div></div>
        <div></div>
        <div></div>
      </div>
    </div>

  )
}

const App = props => {
  const [currPage, setCurrPage] = useState(null)
  const [menuVisibile, setMenuVisibility] = useState(false)
  useEffect(() => {
    // set loading animation
    props.setLoading(true)
    // scroll to top of page
    window.scrollTo(0, 0)
    const pathname = props.history.location.pathname
    const basePathname = pathname.split('/')[1]
    const cp = basePathname.includes('-') ? basePathname.split('-')[0] : basePathname
    // set the state var crrPage
    setCurrPage(cp)

    // setMenuVisibility(false)
  }, [props.history.location.pathname]); 

  return (
    <div className="app-container">
      <header>
        <div className="logo">
          <Link className={currPage === '' ? 'selected' : ''} to="/">
            <img src="/assets/ph_logo2.svg" alt="ph_logo" />
          </Link>
        </div>
        <div 
          className="nav-menu" 
          onClick={() => setMenuVisibility(true)}
        >
          <div>
            <IoMenuOutline />
          </div>
        </div>
        <div 
          className={!menuVisibile ? 'nav-container' : 'nav-container visible'} 
          onClick={() => setMenuVisibility(false)}
        >
          <nav>
            <div className="center-eye">
              <Link className={currPage === '' ? 'selected' : ''} to="/" >
                <img src="/assets/psychic_eye_white.svg" alt="psychic_eye" />
              </Link>
            </div>
            {/*<Link className={currPage === 'artists' ? 'selected' : ''} to="/artists">Artists</Link>*/}
            <Link className={currPage === 'releases' ? 'selected' : ''} to="/releases">
              <span className="nav-main-title">Products</span>
              <span className="nav-eye"></span>
            </Link>
            {/*<Link className={currPage === 'singles-series' ? 'selected' : ''} to="/singles-series">Singles Series</Link>*/}
            <Link className={currPage === 'journal' ? 'selected' : ''} to="/journal">
              <span className="nav-main-title">Journal</span>
              <span className="nav-eye"></span>
            </Link>
            <Link className={currPage === 'tour' ? 'selected' : ''} to="/tour">
              <span className="nav-main-title">Tour</span>
              <span className="nav-eye"></span>
            </Link>
            <div 
              className="menu-close" 
              onClick={() => setMenuVisibility(false)}
            >
              <IoCloseOutline />
            </div>
          </nav>
          
        </div>
        <div className="cart-container">
          <CartNav currPage={currPage} />
        </div>
      </header>
      <main className={props.loading ? 'loading' : ''}>
        <Route exact path="/" component={Home} />
        {/*<Route exact path="/artists" component={Artists} />*/}
        <Route exact path="/journal" component={Journal} />
        {/*<Route exact path="/singles-series" component={SinglesSeries} />*/}
        <Route exact path="/releases" component={Releases} />
        <Route exact path="/tour" component={Tour} />
        <Route exact path="/cart" component={Cart} />
        <Route path={`/artists/:artistSlug`} component={ArtistDetail}/>
        <Route path={`/journal/:entrySlug`} component={JournalDetail}/>
        <Route path={`/categories/:categorySlug`} component={CategoryList}/>
        <Route path={`/journal-author/:authorSlug`} component={JournalAuthorList}/>
        <Route path={`/releases/:releaseSlug`} component={ReleaseDetail}/>
        
      </main>
      <Loader />
      <Footer />
      <VideoIframeContainer iFrameSrc={props.iFrameSrc} />
    </div>
  )
}

const mapStateToProps = ({ counter }) => ({
  loading: counter.loading,
  iFrameSrc: counter.iFrameSrc
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLoading
    },
    dispatch
  )

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(App))
