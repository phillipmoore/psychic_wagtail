import React from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { apiUrl } from '../../utils'
import { setLoading } from '../../modules/counter'

import './categoryList.css'
import { ArtistTitleSplit } from '../releases/releaseListLink'

class JournalCategoryList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      JournalEntries: [],
    }
    this.pathnameToSlug = this.pathnameToSlug.bind(this);
    this.pathnameToName = this.pathnameToName.bind(this);
    this.pageTypeToName = this.pageTypeToName.bind(this);
  }

  pathnameToSlug() {
    const pathnameArray = this.props.location.pathname.split('/').filter(Boolean)
    return pathnameArray[pathnameArray.length - 1]
  }

  pathnameToName() {
    const pathnameArray = this.props.location.pathname.split('/').filter(Boolean)
    return pathnameArray[pathnameArray.length - 1].replace('-', ' ')
  }

  pageTypeToName (pagetype) {
    return pagetype.replace('page', '')
  }

  componentDidMount () {
    // const catSlug = this.props.location.state.id
    const getURL = `${apiUrl}/psychicpages/?categories=${this.pathnameToSlug()}`
    axios.get(getURL).then(res => {
      this.setState({ JournalEntries: res.data.items })
      this.props.setLoading(false)
    })
  }

  render () {
    return (
      <div>
        <h1>
          <div className="h1-subtext">category</div>
          {this.pathnameToName()}
        </h1>
        <ul className="category-list">
        {this.state.JournalEntries.map( (entry, index) => (
          <li 
            key={`entry_${index}`}
            className={`category-list-entry ${entry.content_type__model}`}
          >
            <div>
              <Link to={entry.url_path}>
                <div className="category-list-img-wrap">
                  <img src={entry.category_image__file} alt={entry.title} />
                </div>
                <h4 className="h4-full-width"><ArtistTitleSplit title={entry.title} /></h4>
                <p><ArtistTitleSplit title={this.pageTypeToName(entry.content_type__model)} /></p>
              </Link>
            </div>
          </li>
        ))}
        </ul>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLoading
    },
    dispatch
  )

export default connect(
  null,
  mapDispatchToProps
)(JournalCategoryList)
