import React from 'react'
import { Link } from 'react-router-dom'

import './categoryWidget.css'

class CategoryWidget extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      relatedPages: []
    }
  }

  componentDidMount () {
    // get related pages by categories
    // this.props.categories.forEach(category => {
    //   axios.get(`${apiUrl}/psychicpages/?categories=${category.id}`).then(res => {
    //     const allButThisPage = res.data.items.filter(page => { return parseInt(page.id) !== this.props.currPageId }) 
    //     const updatedRelatedPages = this.state.relatedPages.concat(allButThisPage)
    //     this.setState({relatedPages: updatedRelatedPages})
    //   })
    // })
  }

  render () {
    return (
      <div className="category-widget-component">
        { this.props.categories.length > 0 &&
          <div className="category-widget-container">
            <p className="category-widget-title">categories</p>
            <p className="category-widget-links">
              {this.props.categories.map( (cat, i) => (
                <Link 
                  to={`/categories/${cat.slug}`}
                  key={`entry_cat_${i}`}
                >{cat.name}</Link> 
              ))}
            </p>
          </div>
        }
      </div>
    )
  }
}

export default CategoryWidget