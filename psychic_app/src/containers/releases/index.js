import _ from 'lodash'
import axios from 'axios'
import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'


import { IoSearchOutline } from 'react-icons/io5'

import { apiUrl } from '../../utils'

import { setLoading } from '../../modules/counter'

import './releases.css'

import ProductList from './productList'

class ReleaseList extends React.Component {
  constructor(props) {
    super(props);
    this.inputRef = React.createRef();
    this.state = {
      error: false,
      loading: false,
      products: [],
      limit: 8,
      searchTerm: "",
      releaseType: "",
      releaseTypeDict: {
        PH: 'PH Releases',
        OR: 'Other Releases',
        SS: 'Singles Series',
        SG: 'Soft Goods',
      },
      isFilterMenuShowing: false

    }
    window.onscroll = () => {
      const {
        loadReleases,
        state: { error, loading, hasMore }
      } = this
      if (error || loading || !hasMore) return
      const atBottomOfPage = window.innerHeight ?
        (window.innerHeight + window.pageYOffset) >= document.body.offsetHeight - 2 :
        document.documentElement.scrollHeight - document.documentElement.scrollTop === document.documentElement.clientHeight

      if (atBottomOfPage) {
        // call some loading method
        loadReleases()
      }
    }
    this.loadReleases = this.loadReleases.bind(this)
    this.handleTypeClick = this.handleTypeClick.bind(this)
    this.handleClearType = this.handleClearType.bind(this)
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleInputChangeDebounced = this.handleInputChangeDebounced.bind(this)
    this.handleClearSearchTerm = this.handleClearSearchTerm.bind(this)
    this.handleInputClick = this.handleInputClick.bind(this)
    this.toggleFilters = this.toggleFilters.bind(this)
  }

  componentDidMount () {
    this.loadReleases()
  }

  loadReleases = (searchingOrFiltering = false) => {
    this.setState({ loading: true }, () => {
      // search
      const { products, limit, searchTerm } = this.state
      const searchQuery = !!searchTerm ? `&search=${searchTerm}` : ""
      // filter
      const { releaseType } = this.state
      const filterQuery = !!releaseType && !searchTerm ? `&release_types=${releaseType}` : ""
      const offset = searchingOrFiltering ? 0 : products.length
      axios.get(
          `${apiUrl}/releasepages/?fields=_,id,title,slug,artist,alt_artists,release_image&remove_listing=false&limit=${limit}&offset=${offset}${searchQuery}${filterQuery}`
        )
        .then(res => {
          const newProducts = searchingOrFiltering ? res.data.items : [...this.state.products, ...res.data.items]
          const hasMore = newProducts.length < res.data.meta.total_count
          this.setState({
            hasMore,
            loading: false,
            products: newProducts
          })
          this.props.setLoading(false)
        })
        .catch(err => {
          this.setState({
            error: err.message,
            loading: false
          })
        })
    })
  }

  handleTypeClick (type) {
    this.setState({releaseType: type})
    this.loadReleases(true)
  }

  handleClearType (e) {
    this.setState({releaseType: ''})
    this.loadReleases(true)
  }

  handleClearSearchTerm (e) {
    this.inputRef.current.value = ""
    this.setState({searchTerm: ""})
    this.loadReleases(true)
  }

  handleInputChange (e) {
    this.handleInputChangeDebounced(e.target.value)
  }

  handleInputChangeDebounced = _.debounce((searchTerm) => {
    this.setState({releaseType: ''}) // cannot filter by releaseType and search, so clear releaseType
    this.setState({ searchTerm: searchTerm })
    this.loadReleases(true)    
  }, 300)

  handleInputClick (e) {
    e.target.select();
  }

  toggleFilters() {
    if (this.state.isFilterMenuShowing) {
      this.setState({isFilterMenuShowing: false})
    } else {
      this.setState({isFilterMenuShowing: true})
    }
  }

  render () {
    return (
      <div className="releases-container">
        <h1 className="search-headline">
          <span className="search-headline-text">Products</span>
          <span className="headline-search">
            <input 
              ref={this.inputRef} 
              className={this.state.searchTerm && 'searching'} 
              onChange={this.handleInputChange}
              onClick={this.handleInputClick} 
              type="text" 
            />
            <IoSearchOutline />
          </span> 
        </h1>
        <div className={!this.state.isFilterMenuShowing ? 'releases-filter-bar' : 'releases-filter-bar show'}>
          <div
            onClick={this.toggleFilters}
            className="releases-fb-title"> <p>Filter</p>&nbsp;
          </div>
          <div className="releases-fb-list">
            <label>
              <input 
                type="radio" 
                name="productOptions" 
                defaultChecked={!this.state.releaseType}
                onClick={this.handleClearType}
              />
              <span></span>
              All
            </label>
            {Object.entries(this.state.releaseTypeDict).map( 
                ([key, value]) => (
                  <label key={`radio_${key}`}>
                    <input 
                      type="radio" 
                      name="productOptions" 
                      defaultChecked={this.state.releaseType === key}
                      onClick={() => this.handleTypeClick(key)}
                    />
                    <span></span>
                    {value}
                  </label>
                ) 
              )
            }
          </div>
        </div>
        {this.state.products.length > 0 && 
          <ProductList products={this.state.products} />
        }
      </div>
    )
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLoading
    },
    dispatch
  )

export default connect(
  null,
  mapDispatchToProps
)(ReleaseList)
