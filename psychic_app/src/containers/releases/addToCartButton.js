import React from 'react'
import { connect } from 'react-redux'
import { addToCart } from '../../modules/counter'

class AddToCartButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
    this.noInventory = this.noInventory.bind(this)
  }

  noInventory (id, inventoryQuantity, digital) {
    let condition = false
    if (inventoryQuantity !== undefined && !digital) {
      const itemInCart = this.props.cart.find(item => item.id === id)
      // if this item is in cart check the quantity otherwise check the inventory quantity
      condition = itemInCart ? itemInCart.quantity >= itemInCart.inventoryQuantity : inventoryQuantity <= 0
    }
    return condition
  }

  render () {
    const noInventory = this.noInventory(this.props.id, this.props.inventoryQuantity, this.props.digital)
    const noProduct = !this.props.id
    const buttonText = !noInventory ? (noProduct ? "Choose Item" : "Add to Cart") : "Sold Out"
    return (
      <div className="button-container">
        <button
          disabled={noInventory || noProduct}
          onClick={() => this.props.addToCart({
            id: this.props.id,
            title: this.props.title,
            image: this.props.image,
            price: this.props.price,
            variantTitle: this.props.variantTitle,
            inventoryQuantity: this.props.inventoryQuantity,
            digital: this.props.digital,
            quantity: this.props.quantity,
          })}
          data-id={this.props.id}
        >
          {buttonText}
        </button>
      </div>
    )
  }
}

const mapStateToProps = ({ counter }) => ({
  cart: counter.cart
})

const mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (item) => dispatch(addToCart(item))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddToCartButton)