import React from 'react'
import { connect } from 'react-redux'
import {
  addToCart,
  addReleaseUrl
} from '../../modules/counter'

import ReleaseListLink from './releaseListLink'

import './productList.css'


class ProductList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render () {
    return (
      <div>
        {this.props.products && 
          <div className="product-list">
            {this.props.products.map( (product, index) => (
              <ReleaseListLink key={`product_${index}`} product={product} />
            ))}
          </div>
        }
      </div>
    )
  }
}

const mapStateToProps = ({ counter }) => ({
  cart: counter.cart
})

const mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (item) => dispatch(addToCart(item)),
    addReleaseUrl: (url) => dispatch(addReleaseUrl(url))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductList)
