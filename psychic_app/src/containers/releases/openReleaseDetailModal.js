import React from 'react'

const OpenReleaseDetailModal = (props) => (
  <button
    onClick={() => props.parentprops.addReleaseUrl(props.product.product_link)}
  >
    See Detail
  </button>
)

export default OpenReleaseDetailModal
