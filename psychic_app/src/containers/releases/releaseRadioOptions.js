import React from 'react'
import { connect } from 'react-redux'
import {
  addToCart,
  addReleaseUrl
} from '../../modules/counter'
import { convertAmbientPrice } from '../../utils'

const digital = ['WAV', 'MP3']

class ReleaseOptions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIdOption: 0,
      variantTitle: "",
      inventoryQuantity: 0,
      digital: false,
      quantity: 0
    }
    this.handleSelectChange = this.handleSelectChange.bind(this);
    this.isDigital = this.isDigital.bind(this);
  }

  isDigital (variant) {
    let isDigital = false

    if (variant.sku.toLowerCase().includes('digital') || 
        variant.sku.toLowerCase().includes('mp3') ||
        variant.sku.toLowerCase().includes('wav') ||
        digital.includes(variant.option1)) 
    {
      isDigital = true
    }
    return isDigital
  }

  componentDidMount () {
    const variants = this.props.product.variants
    if(variants.length > 1) {
      const foundIndex = variants.findIndex(v => v.inventory_quantity > 0)
      const firstValidIndex = foundIndex !== -1 ? foundIndex : 0
      this.setState({
        selectedIdOption: variants[firstValidIndex].id,
        variantTitle: variants[firstValidIndex].title,
        inventoryQuantity: variants[firstValidIndex].inventory_quantity,
        digital: this.isDigital(variants[firstValidIndex]) 
      })
    }
  }

  handleSelectChange (e) {
    this.setState({
      selectedIdOption: parseInt(e.target.value),
      variantTitle: e.target.options[e.target.selectedIndex].dataset.title,
      inventoryQuantity: parseInt(e.target.options[e.target.selectedIndex].dataset.quantity)
    })
  }

  render () {
    return (
      <div>
        {this.props.product.variants.length === 1 ?
          (   
            <div>
              {this.props.product.variants.map( (variant, i) => (
                <div key={`variant_${i}`}>
                  <label
                    onClick={() => this.props.selectProduct({
                      productId: this.props.product.id,
                      id: variant.id,
                      title: this.props.product.title,
                      image: this.props.product.featured_image,
                      price: convertAmbientPrice(this.props.product.price),
                      variantTitle: "",
                      inventoryQuantity: variant.inventory_quantity,
                      digital: this.isDigital(variant),
                      quantity: 1,
                    })}
                  >
                    <input type="radio" name="productOptions" />
                    <span></span>
                    {this.props.product.title} ${convertAmbientPrice(this.props.product.price)}
                  </label>
                </div>
              ))}
            </div>
          ) : (
            <div>
              <div>
                {this.props.product.variants.map( (variant, i) => (
                  <label
                    onClick={() => this.props.selectProduct({
                      productId: this.props.product.id,
                      id: variant.id,
                      title: `${this.props.product.title} - ${variant.title}`,
                      image: this.props.product.featured_image,
                      price: convertAmbientPrice(this.props.product.price),
                      variantTitle: "",
                      inventoryQuantity: variant.inventory_quantity,
                      digital: this.isDigital(variant),
                      quantity: 1,
                    })}
                    key={`variant_${i}`}
                  >
                    <input type="radio" name="productOptions" />
                    <span></span>
                    {this.props.product.title} - {variant.title} ${convertAmbientPrice(this.props.product.price)}
                  </label>
                ))}
              </div>
            </div>
          )
        }
      </div>
    )
  }
}

const mapStateToProps = ({ counter }) => ({
  count: counter.count,
  isIncrementing: counter.isIncrementing,
  isDecrementing: counter.isDecrementing,
  cart: counter.cart
})

const mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (item) => dispatch(addToCart(item)),
    addReleaseUrl: (url) => dispatch(addReleaseUrl(url))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReleaseOptions)


/*<button
                  disabled={this.disableButton(variant)}
                  key={`one_button_${i}`}
                  onClick={() => this.props.addToCart({
                    id: variant.id,
                    title: this.props.product.title,
                    image: this.props.product.featured_image,
                    price: convertAmbientPrice(this.props.product.price),
                    variantTitle: "",
                    inventoryQuantity: variant.inventory_quantity,
                    quantity: 1
                  })}
                  data-id={variant.id}
                >
                  Add to CURT
                </button>*/

/*<button 
                disabled={this.disableButton(this.state.selectedIdOption)} 
                onClick={() => this.props.addToCart({
                  id: this.state.selectedIdOption,
                  title: this.props.product.title,
                  image: this.props.product.featured_image,
                  price: convertAmbientPrice(this.props.product.price),
                  variantTitle: this.state.variantTitle,
                  inventoryQuantity: this.state.inventoryQuantity,
                  quantity: 1
                })} 
                data-id=""
              >
                Add to Cart
              </button>*/