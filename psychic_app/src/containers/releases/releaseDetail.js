import React from 'react'
import axios from 'axios'
import _ from 'lodash'
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {
  addReleaseUrl,
  removeReleaseUrl,
  setLoading
} from '../../modules/counter'

import { apiUrl, mainUrl } from '../../utils'

import './releaseDetail.css'

import AddToCartButton from './addToCartButton'
import ReleaseRadioOptions from './releaseRadioOptions'
import { ArtistTitleSplit } from  './releaseListLink'
import CategoryWidget from '../categories/categoryWidget'

const ContentDetail = ({content}) => {
  return (
    <div className="detail-content-detail">
      {content.map( (c, index) => ( 
        <div
          key={`content_${index}`}
          dangerouslySetInnerHTML={{ __html: c.value }} 
        />
      ))}
    </div>
  )
}

class ReleaseDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      releaseTitle: "",
      content: [],
      releaseType: "",
      secondContent: [],
      thirdContent: [],
      products: [],
      images: [],
      ambientBlocks: [],
      selectedProduct: {},
      imagesLoadedCount: 0,
      imagesHolder: [],
      categories: []
    }
    this.returnImageObject = this.returnImageObject.bind(this)
    this.rebuildProduct = this.rebuildProduct.bind(this)
    this.selectProduct = this.selectProduct.bind(this)
    this.loadImage = this.loadImage.bind(this)
  }

  returnImageObject (id, name, src) {
    return {
      productId: id,
      productName: name,
      src: src
    }
  }

  loadImage (src) {
    // a function that preloadeds ambient images
    const img = new Image();
    img.onload = (e, src) => {
      const loadedCount = this.state.imagesLoadedCount + 1 
      this.setState({imagesLoadedCount: loadedCount })
    }
    img.src = src
  }

  rebuildProduct (res) {
    // a method that lumps all the images provided by ambient blocks as objects in one array
    const releaseImageArray = []

    // only add the featured image if it is different from the first ambient image
    if (!res.data.same_first_image) {
      const firstImageSrc = `${mainUrl}${res.data.release_image.url}`
      releaseImageArray.push(this.returnImageObject(undefined, undefined, firstImageSrc))
    }

    const blocks = res.data.ambient_blocks
    for (let i = 0; i < blocks.length; i++) {
      const product = blocks[i].value.product
      const productImageArray = product.images.map((img, index) => {
        // calling this method preloads the images
        this.loadImage(img)
        return this.returnImageObject(product.id, product.title, img)
      })

      releaseImageArray.push(...productImageArray)
    }
    return releaseImageArray
  }

  selectProduct (product) {
    const slideNum = this.state.images.findIndex(img => img.productId === product.productId)
    this.setState({currentSlide: slideNum})
    this.setState({selectedProduct: product})
  }

  componentDidUpdate(prevProps) {
    const lc = this.state.imagesLoadedCount
    // wait until all ambient images have loaded before changing adding to images array
    if (lc > 0 && lc === this.state.images.length && this.state.images.length === 1) {
      // give it an extra little bit to be sure
      setTimeout(()=>{
        this.setState({ images: this.state.imagesHolder })
      }, 200)
    }
  }

  componentDidMount () {
    // make first call to api
    axios.get(`${apiUrl}/pages/find/?html_path=${this.props.location.pathname}&fields=_,id,title,artist,alt_artists,release_image,content,second_content,third_content,categories`).then(firstResponse => {
      const artistTitle = firstResponse.data.artist ? firstResponse.data.artist.title : firstResponse.data.alt_artists
      this.setState({ title: artistTitle })
      this.setState({ releaseTitle: firstResponse.data.title })
      this.setState({ content: firstResponse.data.content })
      this.setState({ releaseType: firstResponse.data.release_type })
      this.setState({ secondContent: firstResponse.data.second_content })
      this.setState({ thirdContent: firstResponse.data.third_content })
      this.setState({ categories: firstResponse.data.categories })
      const firstImageSrc = `${mainUrl}${firstResponse.data.release_image.url}`
      this.setState({ images: [this.returnImageObject(undefined, undefined, firstImageSrc)] })

      this.props.setLoading(false)

      // if firstResponse all good and done, make second api call to get ambient products
      axios.get(`${apiUrl}/pages/find/?html_path=${this.props.location.pathname}&fields=_,featured_image,same_first_image,ambient_blocks`).then(secondRespone => {
        this.setState({ ambientBlocks: secondRespone.data.ambient_blocks })

        const rebuiltImages = this.rebuildProduct(secondRespone)
        this.setState({ imagesHolder: rebuiltImages })
      })
    })
  }

  render () {
    return (
      <div className={_.isEmpty(this.state.data) ? 'release-detail' : 'release-detail show'}>
        <div className="detail-container">
          <div className="detail-images">
            <div className="detail-left-headline">
              <h1 className="h4-full-width">
                {!!this.state.title && <ArtistTitleSplit title={this.state.title} />}
              </h1>
              <h2 className="h4-full-width">
                {!!this.state.releaseTitle && <ArtistTitleSplit title={this.state.releaseTitle} />}
              </h2>
            </div>
            <Carousel
              showStatus={false}
              showIndicators={false}
              selectedItem={this.state.currentSlide}
              showArrows={false} 
            >
              {this.state.images.map( (image, index) => (
                <span 
                  className="product-image"
                  key={`image_${index}`}
                >
                  <img src={image.src} alt="" />
                </span>
              ))}
            </Carousel>
          </div>
          <div className="detail-description">
            <div className="detail-right-headline">
              <h1 className="h4-full-width">
                {!!this.state.title && <ArtistTitleSplit title={this.state.title} />}
              </h1>
              <h2 className="h4-full-width">
                {!!this.state.releaseTitle && <ArtistTitleSplit title={this.state.releaseTitle} />}
              </h2>
            </div>
            <div className="detail-content-buy-options">
              <ContentDetail content={this.state.content} />
              {this.state.title &&
                <div className="detail-buy-options">
                  <div className="detail-format-title">
                    { this.state.releaseType === 'ME' ?
                    <span>Sizes:</span> :
                    <span>Formats:</span>
                    }
                  </div>
                  <div className="detail-radio-options">
                    {this.state.ambientBlocks.map( (block, index) => ( 
                      <ReleaseRadioOptions
                        key={`block_${index}`}
                        product={block.value.product}
                        selectProduct={this.selectProduct} 
                      />
                    ))}
                  </div>
                  <AddToCartButton
                    id={this.state.selectedProduct.id}
                    title={this.state.selectedProduct.title}
                    image={this.state.selectedProduct.image}
                    price={this.state.selectedProduct.price}
                    variantTitle={this.state.selectedProduct.variantTitle}
                    inventoryQuantity={this.state.selectedProduct.inventoryQuantity}
                    digital={this.state.selectedProduct.digital}
                    quantity={this.state.selectedProduct.quantity}
                  />
                </div>
              }
            </div>
          </div>
        </div>
        <div className="lower-detail-container">
          <ContentDetail content={this.state.secondContent} />
          <ContentDetail content={this.state.thirdContent} />
        </div>
        <CategoryWidget categories={this.state.categories} />
      </div>
    )
  }
}


const mapStateToProps = ({ counter }) => ({
  releaseUrl: counter.releaseUrl
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addReleaseUrl,
      removeReleaseUrl,
      setLoading
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReleaseDetail)
