import React from 'react'
import { connect } from 'react-redux'
import {
  addToCart
} from '../../modules/counter'

class SelectProductButton extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // selectedIdOption: 0,
      // variantTitle: "",
      // inventoryQuantity: 0,
      // quantity: 0
    }
    // this.handleSelectChange = this.handleSelectChange.bind(this);
    this.disableButton = this.disableButton.bind(this)
  }

  disableButton (id, inventoryQuantity, digital) {
    let condition = false
    if (inventoryQuantity !== undefined && !digital) {
      const itemInCart = this.props.cart.find(item => item.id === id)
      // if this item is in cart check the quantity otherwise check the inventory quantity
      condition = itemInCart ? itemInCart.quantity >= itemInCart.inventoryQuantity : inventoryQuantity <= 0
    }

    console.log(condition)
    
    return condition
  }

  render () {
    return (
      <button
        disabled={this.disableButton(this.props.id, this.props.inventoryQuantity, this.props.digital)}
        onClick={() => this.props.selectProduct({
          productId: this.props.productId,
          id: this.props.id,
          title: this.props.title,
          image: this.props.image,
          price: this.props.price,
          variantTitle: this.props.variantTitle,
          inventoryQuantity: this.props.inventoryQuantity,
          digital: this.props.digital,
          quantity: this.props.quantity,
        })}
        data-id={this.props.id}
      >
        {this.props.title}
      </button>
    )
  }
}

const mapStateToProps = ({ counter }) => ({
  cart: counter.cart
})

const mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (item) => dispatch(addToCart(item))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectProductButton)