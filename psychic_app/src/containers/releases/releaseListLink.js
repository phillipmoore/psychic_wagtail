import React from 'react'
import { Link } from 'react-router-dom'

import { mainUrl } from '../../utils'


export class ArtistTitleSplit extends React.Component {
  render() {
    let returnValue = ''
    if (this.props.title) {
      const arrayToMap = this.props.title.includes(' ') ? this.props.title.split(' ') : [...this.props.title]
      returnValue = (
        <span className={this.props.title.includes(' ') ? 'split-words' : 'split-letters'}>
          {arrayToMap
            .map((item, i) => (
              <span key={`item_${i}`}>
                {item}
              </span>)
            ).reduce((prev, curr) => [prev, '', curr])
          }
        </span>
      )
    }
    return returnValue
  }
}

class ReleaseListLink extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  render () {
    return (
      <div className="product">
        <Link to={`/releases/${this.props.product.meta.slug}`} >
          <div className="product-detail-img-wrap">
            <div>
              { this.props.product.release_image &&
                <img src={`${mainUrl}${this.props.product.release_image.url}`} alt={this.props.product.title} />
              }
            </div>
          </div>
          { this.props.product.artist ?
            <h4 className="h4-full-width"><ArtistTitleSplit title={this.props.product.artist.title} /></h4> :
            <h4 className="h4-full-width"><ArtistTitleSplit title={this.props.product.alt_artists} /></h4>
          }
          <p>{this.props.product.title}</p>
        </Link>
      </div>
    )
  }
}

export default ReleaseListLink