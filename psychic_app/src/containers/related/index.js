import React from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'

import { apiUrl } from '../../utils'
import './related.css'

class RelatedContent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      relatedPages: []
    }
  }

  componentDidMount () {
    // get related pages by categories
    this.props.categories.forEach(category => {
      axios.get(`${apiUrl}/psychicpages/?categories=${category.id}`).then(res => {
        const allButThisPage = res.data.items.filter(page => { return parseInt(page.id) !== this.props.currPageId }) 
        const updatedRelatedPages = this.state.relatedPages.concat(allButThisPage)
        this.setState({relatedPages: updatedRelatedPages})
      })
    })
  }

  render () {
    return (
      <div className="related-component">
        <h3>RELATED</h3>
        <div className="related-content-container">
          <div className="related-content">
            <ul>
              {this.state.relatedPages.map( (relatedPage, index) => (
                <li 
                  key={`relatedPage_${index}`}
                  className={relatedPage.content_type__model}
                >
                  <Link to={relatedPage.url_path} >
                    <h4>{relatedPage.title}</h4>
                    <div className="related-img-container">
                      <div>
                        <div className="img-wrap">
                          <svg viewBox="0 0 16 9" ></svg>
                          <img src={relatedPage.category_image__file} alt={relatedPage.title} />
                        </div>
                      </div>
                    </div>
                    <p className="page-type">{relatedPage.content_type__model.replace('page', '')}</p>
                  </Link>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    )
  }
}

export default RelatedContent