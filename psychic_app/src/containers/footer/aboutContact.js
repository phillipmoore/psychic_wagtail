import React from 'react'

import './aboutContact.css'

import { IoCloseOutline } from 'react-icons/io5'

const CopiedText = () => {
  return (
    <span className="copied-text">
      <span>C</span>
      <span>o</span>
      <span>p</span>
      <span>i</span>
      <span>e</span>
      <span>d</span>
    </span>
  )
}

class AboutContact extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      emails: [
        'info@psychic-hotline.net',
        'promo@psychic-hotline.net',
        'licensing@psychic-hotline.net',
      ]
    }
    this.mailToClipboard = this.mailToClipboard.bind(this)
  }

  mailToClipboard (e) {
    const _addRemoveCopied = (el) => {
      el.classList.add('copied')
      setTimeout(()=>{
        el.classList.remove('copied')
      }, 1000)
    }

    const emailInput = e.target.lastChild
    emailInput.select()
    emailInput.setSelectionRange(0, 99999)
    if (navigator.clipboard !== undefined) { // Chrome, etc.
        navigator.clipboard.writeText(emailInput.value)
        _addRemoveCopied(e.target)
    }
    else if(window.clipboardData) { // Internet Explorer
        window.clipboardData.setData("Text", emailInput.value)
        _addRemoveCopied(e.target)
    }
  }

  render () {
    return (
      <div className={!!this.props.link ? `about-contact-container show` : `about-contact-container`}>
        <div className="about-contact">
          <div className="toggle-about-contact">
            <div className={this.props.link === "about" ? 'about show' : 'about'}>
              <h2>About</h2>
              <div>
                <p>Psychic Hotline is an artist-run recording company based in Durham, NC, founded by Amelia Meath & Nick Sanborn of Sylvan Esso alongside their longtime manager, Martin Anderson. 
                  Working with Secretly Distribution and Kris Chen (Nonesuch, XL, Domino), the company seeks to create new works and reissue classic albums from their ever-expanding musical community. 
                  Led by the artist perspective, Psychic Hotline’s aim is to make the process of creating and releasing albums accessible to those who make them-reflected in radically artist-forward dealmaking, creative support, and innovative marketing approaches.
                </p>
                <p>We created Psychic Hotline to be the type of record company we had dreamed of. 
                  An artist led enterprise that strives to do right in radical ways by the creators who entrust us with their work; in the deals we make, the way we do business and the way we center the art and the artist. 
                  We do not seek to own intellectual property under any circumstance, nor do we subscribe toscarcity based career approaches. 
                  We strive to ever-expand our musical community; to welcome more voices and more perspectives. We believe that the only common denominator in great music is honesty in expression. 
                  It is in this spirit that we welcome youall to Psychic Hotline.
                </p>
              </div>
            </div>
            <div className={this.props.link === "contact" ? 'contact show' : 'contact'}>
              <h2>Contact</h2>
              <div className="email-links">
                {this.state.emails.map((email, index) => (
                    <p key={`email_${index}`}>
                      <span 
                        onMouseUp={this.mailToClipboard}
                        className="email-link"
                      >
                        {email}
                        <input type="text" defaultValue={email} />
                      </span>
                      <CopiedText />
                    </p>
                  )
                )}
                <p><a href="tel:1-888-PSY-HTLN">1-888-PSY-HTLN</a></p>
              </div>
              {/*<div className="email-message">
                <p>* We are not currently accepting submissions.</p>
              </div>*/}
            </div>
          </div>
          <div className="about-contact-close">
            <a onClick={this.props.handleLink}><IoCloseOutline /></a>
          </div>
        </div>
      </div>
    )
  }
}

export default AboutContact;
