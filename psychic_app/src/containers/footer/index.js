import React, { useState, useEffect, useRef } from 'react'
import { 
  IoLogoFacebook, 
  IoLogoTwitter,
  IoLogoInstagram,
  IoLogoYoutube 
} from 'react-icons/io5'
import Newsletter from './newsletter'
import AboutContact from './aboutContact'
import './footer.css'

const Footer = props => {
  const [link, setLink] = useState(undefined)
  useEffect(() => {
  }, []); 

  const linksRef = useRef(null);

  const handleLink = (e) => {
    if (e.target.dataset.link) {
      window.scrollTo(0, linksRef.current.offsetTop - linksRef.current.offsetHeight)
      setLink(e.target.dataset.link)
      document.body.style.height = '100%'
      document.body.style.overflow = 'hidden'
    } else {
      setLink(undefined)
      document.body.style.removeProperty("height")
      document.body.style.removeProperty("overflow")
    } 
  }

  return (
    <footer className={!!link ? 'show' : ''} ref={linksRef} >
      <div className="socials">
        <a href="https://www.instagram.com/psychichotline/" target="_blank" rel="noopener noreferrer" alt="insta">
          <IoLogoInstagram />
        </a>
        <a href="http://twitter.com/psyhotline" target="_blank" rel="noopener noreferrer" alt="twitter">
          <IoLogoTwitter />
        </a>
        <a href="http://facebook.com/psychichotlinesounds" target="_blank" rel="noopener noreferrer"alt="facebook">
          <IoLogoFacebook />
        </a>
        <a href="https://www.youtube.com/channel/UCM8VhHVOLBPGXpFkjl6uwCQ" target="_blank" rel="noopener noreferrer" alt="facebook">
          <IoLogoYoutube / >
        </a>
      </div>
      <div className="links">
        <div>
          <p>
            <a onClick={handleLink} data-link="contact">Contact</a>
            <a onClick={handleLink} data-link="about">About</a>
          </p>
        </div>
        <div><img src="/assets/psychic_eye.svg" alt="pshychic_eye_footer" /></div>
        <div className="copyright"><p>&#169; Psychic Hotline, LLC</p></div>
        <AboutContact link={link} handleLink={handleLink}/>
      </div>
      <div className="newsletter">
        <Newsletter />  
      </div>
    </footer>
  )
}

export default Footer
  