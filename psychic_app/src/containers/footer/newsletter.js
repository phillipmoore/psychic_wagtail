import React from 'react'

import MailchimpSubscribe from "react-mailchimp-subscribe"

import './footer.css'

// a basic form
const CustomForm = ({ status, message, onValidated }) => {
  // const [hasSubmitted, setHasSubmitted] = useState(false)
  // useEffect(() => {
  //   setTimeout (()=>{
  //     setHasSubmitted(false)
  //   }, 5000)
  // }, [hasSubmitted]); 

  let email
  const submit = () =>
    email &&
    email.value.indexOf("@") > -1 &&
    onValidated({
      EMAIL: email.value,
    })

  return (
    <div className="form">
      <label><span>LOOK</span><span>OUT</span></label>
      <input
        ref={node => (email = node)}
        type="email"
        placeholder="Email address"
      />
      <div className="button-container">
        <button onClick={submit}>
          Sign Up
        </button>
      </div>
      <p className="sign-up-status">
        {status === "sending" && <div>sending...</div>}
        {status === "error" && (
          <div dangerouslySetInnerHTML={{ __html: message }} />
        )}
        {status === "success" && (
          <div dangerouslySetInnerHTML={{ __html: message }} />
        )}
      </p>
    </div>
  )
}

class Newsletter extends React.Component {
  render () {
    const url = 'https://theglowmgmt.us17.list-manage.com/subscribe/post?u=1143ef2ae4c2f267d492eb9cc&amp;id=dc54622846'
    return (
      <MailchimpSubscribe
        url={url}
        render={({ subscribe, status, message }) => (
          <CustomForm
            status={status}
            message={message}
            onValidated={formData => subscribe(formData)}
          />
        )}
      />
    )
  }
}


export default Newsletter;