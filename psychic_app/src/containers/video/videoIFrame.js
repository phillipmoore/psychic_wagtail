import React, { useEffect } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { removeIFrameSrc } from '../../modules/counter' 

import { IoCloseOutline } from 'react-icons/io5'

import './video.css'

const VideoIframeContainer = props => {
  useEffect(() => {
    if (!!props.iFrameSrc) {

    } 

  }, [props.iFrameSrc]);

  return (
    <div className={ !props.iFrameSrc ? 'video-iframe-container' : `video-iframe-container show-video`}>
      <iframe 
        src={props.iFrameSrc} 
        title="YouTube video player" 
        frameBorder="0" 
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" 
        allowFullScreen
      ></iframe>
      <div 
        onClick={props.removeIFrameSrc}
        className="close"
      >
        {<IoCloseOutline />}
      </div>
    </div>
  )
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      removeIFrameSrc
    },
    dispatch
  )

const mapStateToProps = ({ counter }) => ({
  iFrameSrc: counter.iFrameSrc
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VideoIframeContainer)