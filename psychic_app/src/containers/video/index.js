import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { Link } from 'react-router-dom'

import { IoPlayOutline } from 'react-icons/io5'

import { mainUrl } from '../../utils'

import './video.css'

import { ArtistTitleSplit } from '../releases/releaseListLink'

import { setIFrameSrc } from '../../modules/counter'


class Video extends React.Component {
  constructor(props) {
    super(props);
    this.state = {}
    this.showAndPlayVideo = this.showAndPlayVideo.bind(this)
  }

  showAndPlayVideo = (video) => {
    console.log(video)
    this.props.setIFrameSrc(video.embed_code)
  }

  render () {
    return (
      <div className="video">
        <div 
          className="video-image-container"
          onClick={() => this.props.setIFrameSrc(this.props.video.embed_code)}
        >
          <img src={`${mainUrl}${this.props.video.video_image.url}`} alt={this.props.video.title} />
          <IoPlayOutline />
        </div>
        { this.props.video.artist ?
          <h4 className="h4-full-width"><ArtistTitleSplit title={this.props.video.artist.title} /></h4> :
          <h4 className="h4-full-width"><ArtistTitleSplit title={this.props.video.alt_artists} /></h4>
        }
        <p>{this.props.video.title}</p>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setIFrameSrc
    },
    dispatch
  )

export default connect(
  null,
  mapDispatchToProps
)(Video)
