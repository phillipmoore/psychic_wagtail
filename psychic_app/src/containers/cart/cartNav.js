import React from 'react'
import { Link, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  addToCart,
  removeFromCart
} from '../../modules/counter'
import { IoCartOutline } from 'react-icons/io5'
import './cart.css'
import CartList from './cartList'
// <IoCartOutline />
const CartItemCount = props => (
  <div className="cart-link">
    <Link 
      className={ props.inCart || props.parentProps.cart.length <= 0 ? 'selected' : '' }
      to="/cart"
    >
      <span>
        <IoCartOutline />
        <span>
          (<span className={props.parentProps.cart.length > 0 ? 'with-items' : ''}>
          { props.parentProps.cart.length > 0 ? (
            props.parentProps.cart.map((item) => { 
              return item.quantity 
            }).reduce((prev, next) => prev + next)
            ) : (0)
          }
          </span>)
        </span>
      </span>
    </ Link>
  </div>
)

class CartNav extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inCart: false,
      itemAdded: false,
      cartTimeout: setTimeout(()=>{})
    }
    this.handleOnMouseOut = this.handleOnMouseOut.bind(this)
  }

  componentDidMount () {
    if (this.props.location.pathname === "/cart"){
      this.setState({ inCart: true })
    }
  }

  componentDidUpdate(prevProps) {
    const _reduceQuantity = cart => {
      return cart.length > 0 ? cart.map(item=>item.quantity).reduce((x,y)=> x + y) : 0
    }
    
    if (_reduceQuantity(this.props.cart) !== _reduceQuantity(prevProps.cart)) {
      this.setState({ itemAdded: true })
      const cartTimeout = setTimeout(() =>{
        this.setState({ itemAdded: false })
      }, 2000)
      this.setState({ cartTimeout: cartTimeout})
    }

    if (this.props.location.pathname !== prevProps.location.pathname) {
      if (this.props.location.pathname === "/cart"){
        this.setState({ inCart: true })
      } else {
        this.setState({ inCart: false })
      }
    }
  }

  handleOnMouseOut () {
    this.setState({ itemAdded: false })
    if (this.state.cartTimeout) {
      clearTimeout(this.state.cartTimeout)
    }
  }

  render () {
    return (
      <div className="cart cart-nav">
        <div 
          className={this.state.inCart ? 'cart-nodrop' : (this.state.itemAdded ? 'cart-dropdown open' : 'cart-dropdown')}
          onMouseOut={this.handleOnMouseOut}
        >
          { this.props.cart.length > 0 &&
            <div> 
              <CartList />
              <div className="button-container secondary">
                <Link 
                  to="/cart" 
                  className="cart-proceed button"
                >Proceed to Checkout</Link>
              </div>
            </div>
          }
        </div>
        <CartItemCount parentProps={this.props} inCart={this.state.inCart} />
      </div>
    )
  }
}

const mapStateToProps = ({ counter }) => ({
  cart: counter.cart,
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addToCart,
      removeFromCart
    },
    dispatch
  )

export default  connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(props => <CartNav {...props}/>))