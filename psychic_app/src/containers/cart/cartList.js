import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import {
  addToCart,
  removeFromCart
} from '../../modules/counter'

import './cart.css'

const TotalCost = props => (
  <div>${
    props.cart.length > 0 ? (
      (props.cart.map((item) => { 
        const itemMultiplied = parseFloat(item.price) * item.quantity
        return props.cart.length !== 1 ? itemMultiplied : itemMultiplied
      }).reduce((prev, next) => (prev + next))).toFixed(2)
    ) : ("0.00")
  }</div>
)

class Cart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      
    }
    this.handleAddRemoveClick = this.handleAddRemoveClick.bind(this);
  }

  handleAddRemoveClick (e, add, item) {
    if (add) {
      this.props.addToCart(item)
    } else {
      this.props.removeFromCart(item)
    }
    e.preventDefault()
  }

  render () {
    return (
      <div className="cart-list-container">
        
          <ul className="cart-list">
            <li>
              <div><p>Item</p></div>
              <div className="cart-quantity-label"><p>Quantity</p></div>
              <div><p><span className="alt-q-label">Quantity/</span>Cost</p></div>  
            </li>
          </ul>
          {this.props.cart.length > 0 ? (
            <div className="cart-item-list-container">
              <ul className="cart-list"> 
                {this.props.cart.map( (item, i) => (
                  <li 
                    key={`item_${i}`}
                  >
                    <div>
                      <img src={item.image} alt={item.title} />
                      <span>
                        {item.title}
                        {item.variantTitle && ` - ${item.variantTitle}`}
                      </span>
                    </div>
                    <div className="add-remove-container large">
                      <div>{item.quantity}</div>
                      <div className="add-remove">
                        <span
                          onClick={(e) => this.handleAddRemoveClick(e, false, item)}
                        >-</span>
                        <span
                          onClick={(e) => this.handleAddRemoveClick(e, true, item)}
                        >+</span>
                      </div>
                    </div>
                    <div>
                      <span>${(parseFloat(item.price) * item.quantity).toFixed(2)}</span>
                      <div className="add-remove-container small">
                        <div>{item.quantity}</div>
                        <div className="add-remove">
                          <span
                            onClick={(e) => this.handleAddRemoveClick(e, false, item)}
                          >-</span>
                          <span
                            onClick={(e) => this.handleAddRemoveClick(e, true, item)}
                          >+</span>
                        </div>
                      </div>
                    </div>
                  </li>
                ))}
              </ul>
            </div>
          ) : <li className="empty-cart"><p>Your Cart is Empty</p></li>}
          <ul className="cart-list">
            <li>
              <div className="cart-empty-div"></div>
              <div><p>Subtotal:</p></div>
              <div>
                <TotalCost cart={this.props.cart}/>
              </div>  
            </li>
          </ul>
        
      </div>
    )
  }
}

const mapStateToProps = ({ counter }) => ({
  cart: counter.cart,
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      addToCart,
      removeFromCart
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Cart)