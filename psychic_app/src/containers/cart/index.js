import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'

import CartList from './cartList'

import { setLoading } from '../../modules/counter'

const CheckoutButton = props => (
  <div className="button-container">
    <a 
      className="checkout-button button"
      href={
        'https://ambientmerch.myshopify.com/cart/' + 
        props.cart.map(item => `${item.id}:${item.quantity}`).join() + 
        '?access_token=8e9a23e53334babbd78ea6522197f245&_fd=0'
      }
      target="_blank"
      rel="noopener noreferrer"
    > 
      Checkout
    </a>
  </div>
)

class CartPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checkoutUrl: ""
    }
  }

  componentDidMount () {
    setTimeout(() => {
      this.props.setLoading(false)
    }, 100)
    
  }

  render () {
    return (
      <div className="cart-page">
        <CartList />
        <div className="cart-checkout-bar">
          <div className="button-container secondary">
            <Link to="/releases" className="button">Keep Browsing</Link>
          </div>
          {this.props.cart.length > 0 &&
            <CheckoutButton cart={this.props.cart} />
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = ({ counter }) => ({
  cart: counter.cart,
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLoading
    },
    dispatch
  )

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CartPage)
