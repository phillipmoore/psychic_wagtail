import React from 'react'
import axios from 'axios'

import {apiUrl} from '../../utils'

import ProductList from '../releases/productList'

class SinglesSeriesList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
    }
  }

  componentDidMount () {
    // featured releases are release_type=FE
    axios.get(`${apiUrl}/pages/?type=home.ReleasePage&fields=_,id,title,artist,featured_image&release_type=SS`).then(res => {
      this.setState({ products: res.data.items })
    })
  }

  render () {
    return (
      <div>
        <h1>Singles Series</h1>
        <ProductList products={this.state.products} />
      </div>
    )
  }
}

export default SinglesSeriesList
