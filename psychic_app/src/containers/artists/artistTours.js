import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { setLoading } from '../../modules/counter'

import './artistTours.css'


class ArtistTours extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
    this.resizeIframe = this.resizeIframe.bind(this)
  }

  resizeIframe(obj) {
    obj.target.style.height = obj.target.contentWindow.document.documentElement.scrollHeight + 'px';
  }

  componentDidMount () {
    this.props.setLoading(false)
  }

  render () {
    return (
      <div className="artist-tours">
        <iframe 
          src={`/assets/bit.html?band=${this.props.bandsintown}`}
          onLoad={this.resizeIframe}
          title="artist_tours"
        ></iframe>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLoading
    },
    dispatch
  )

export default connect(
  null,
  mapDispatchToProps
)(ArtistTours)
