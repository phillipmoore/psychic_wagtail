import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { setLoading } from '../../modules/counter'

import './artistVideos.css'


class ArtistVideos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }
  
  componentDidMount () {
    this.props.setLoading(false)
  }

  render () {
    return (
      <ul className="artist-videos">
        {this.props.videos.map( (video, index) => (
          <li 
            className="video"
            key={`video_${index}`}
          >
            <svg viewBox="0 0 16 9"></svg>
            <div dangerouslySetInnerHTML={{ __html: video.value.code }} />                  
          </li>
        ))}
      </ul>
    )
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLoading
    },
    dispatch
  )

export default connect(
  null,
  mapDispatchToProps
)(ArtistVideos)
