import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Route, Link } from 'react-router-dom'
import axios from 'axios'
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from 'react-responsive-carousel'

import { setLoading } from '../../modules/counter'

import './artistDetail.css'

import ArtistReleases from './artistReleases'
import ArtistVideos from './artistVideos'
import ArtistTours from './artistTours'

import { apiUrl } from '../../utils'

const RenderNumberThumbs = (props) => {
  return (
    props.map((_, i) => {
      return (props.length > 1 ? <p>{i+1}</p> : '')
    })
  )
}

class ArtistDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      artistId: "",
      title: "",
      slug: "",
      images: [],
      bio: "",
      externalLinks: [],
      videos: [],
      bandsintown: "",
      products: [],
      internalLinks: ['Releases', 'Videos', 'Tours']
    }
    this.cleanPathname = this.cleanPathname.bind(this)
  }

  cleanPathname () {
    let pathname = this.props.location.pathname
    const pathnameArray = pathname.split('/').filter(Boolean)
    const lastWord = pathnameArray[pathnameArray.length - 1]
    const lowerInternalLinks = this.state.internalLinks.map(link => link.toLowerCase())
    if (lowerInternalLinks.includes(lastWord) && pathnameArray.length > 2) {
      pathnameArray.pop()
      pathname = `/${pathnameArray.join('/')}`
    }
    return pathname
  }

  componentDidMount () {
    axios.get(`${apiUrl}/pages/find/?html_path=${this.cleanPathname()}`).then(res => {
      this.setState({ artistId: res.data.id })
      this.setState({ title: res.data.title })
      this.setState({ slug: res.data.meta.slug })
      this.setState({ images: res.data.images })
      this.setState({ bio: res.data.bio })
      this.setState({ externalLinks: res.data.links })
      this.setState({ videos: res.data.videos })
      this.setState({ bandsintown: res.data.bandsintown })
      const ambientHandle = res.data.ambient_artist_handle
      const products = ambientHandle > 0 ? res.data.ambient_artist_handle[0].value.products : []
      this.setState({ products: products })
      // this.props.setLoading(false)
    })
  }

  render () {
    return (
      <div>
        <h1>{this.state.title}</h1>
        <div className="artist-container">
          <div className="side-1"> 
            <div className="artist-primary-content">
              <div className="artist-gallery pad">
                <ul className="artist-image-list">
                  {this.state.images.map( (img, index) => (
                    <li key={`img_${index}`} >
                      <img src={img.value} alt={`${this.state.title}_img_${index}`} />
                    </li>
                  ))}
                </ul>
                <Carousel
                  className="artist-carousel"
                  showStatus={false}
                  selectedItem={this.state.currentSlide}
                  dynamicHeight={true} 
                  renderThumbs={() => RenderNumberThumbs(this.state.images)}
                  showIndicators={false}
                  thumbWidth={24}
                  showArrows={false}
                  showThumbs={this.state.images.length > 1}
                >
                  {this.state.images.map( (image, index) => (
                    <span 
                      className="artist-image"
                      key={`image_${index}`}
                    >
                      <img src={image.value} alt={`${this.state.title}_carimg_${index}`} />
                    </span>
                  ))}
                </Carousel>
              </div>
              <div className="artist-exlinks-bio pad">
                <div className="artist-external-links">
                  <ul>
                    {this.state.externalLinks.map( (link, index) => (
                      <li 
                        className="link"
                        key={`link_${index}`}
                      >
                        <a href={link.value.url} target="_blank" rel="noopener noreferrer">
                          {link.value.text}
                        </a>
                      </li>
                    ))}
                  </ul>
                </div>
                <div className="artist-bio">
                  <div dangerouslySetInnerHTML={{ __html: this.state.bio }} />
                </div>
              </div>
            </div>
          </div>
          <div className="side-2">
            <div className="artist-secondary-content">
              <div className="artist-secondary-nav">
                <Link to={`/artists/${this.state.slug}`}>{this.state.internalLinks[0]}</Link>
                <Link to={`/artists/${this.state.slug}/videos`}>{this.state.internalLinks[1]}</Link>
                <Link to={`/artists/${this.state.slug}/tours`}>{this.state.internalLinks[2]}</Link>
              </div>
              <div className="artist-secondary-routes pad">
                <Route exact path={`/artists/:artistSlug`} component={() => <ArtistReleases artistId={this.state.artistId} />} />
                <Route path={`/artists/:artistSlug/videos`} component={() => <ArtistVideos videos={this.state.videos} artistId={this.state.artistId} />} />
                <Route path={`/artists/:artistSlug/tours`} component={() => <ArtistTours bandsintown={this.state.bandsintown} artistId={this.state.artistId} />} />
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLoading
    },
    dispatch
  )

export default connect(
  null,
  mapDispatchToProps
)(ArtistDetail)
