import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import axios from 'axios'

import { setLoading } from '../../modules/counter'

import './artists.css'

import { apiUrl } from '../../utils'

class ArtistList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      artists: [],
      visibleArtist: ""
    }
    this.handleMouseOver = this.handleMouseOver.bind(this)
    this.handleMouseOut = this.handleMouseOut.bind(this)
  }

  componentDidMount () {
    axios.get(`${apiUrl}/pages/?type=home.ArtistPage&fields=_,id,title,slug,images`).then(res => {
      this.setState({ artists: res.data.items })
      this.props.setLoading(false)
    })
  }

  handleMouseOver (artist) {
    this.setState({ visibleArtist: artist.title })
  }

  handleMouseOut () {
    this.setState({ visibleArtist: "" })
  }

  render () {
    return (
        <div className="artist-list-component">
          <h1>Artists</h1>
          <ul className="artist-list pad">
            {this.state.artists.map( (artist, index) => (
              <li 
                className="artist-card"
                key={`artist_${index}`}
                onMouseOver={() => this.handleMouseOver(artist)}
                onMouseOut={this.handleMouseOut}
              >
                <div>
                  <div><h4 className="artist-title">{artist.title}</h4>
                    <Link to={`/artists/${artist.meta.slug}`}>
                        <div className="artist-img-container">
                          { artist.images.length > 0 &&
                            <img src={artist.images[0].value} alt={artist.title}/>
                          }
                      </div>
                    </Link>
                  </div>
                </div>
              </li>
            ))}
          </ul>
          <h2 className="visible-artist">{this.state.visibleArtist}</h2>
        </div>
    )
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLoading
    },
    dispatch
  )

export default connect(
  null,
  mapDispatchToProps
)(ArtistList)
