import axios from 'axios'
import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { setLoading } from '../../modules/counter'

import '../releases/productList.css'
import ProductList from '../releases/productList'
import { apiUrl } from '../../utils'



class ArtistReleases extends React.Component {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      products: []
    }
  }

  componentDidMount () {
    this._isMounted = true;
    if (this.props.artistId) {
      axios.get(`${apiUrl}/pages/?type=home.ReleasePage&fields=_,id,title,slug,artist,alt_artists,release_image&artist=${this.props.artistId}`).then(res => {
        if (this._isMounted) {
          this.setState({ products: res.data.items })
          this.props.setLoading(false)
        }
      })
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render () {
    return (
      <div className="artist-release-container">
        <ProductList products={this.state.products} />
      </div>
    )
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLoading
    },
    dispatch
  )

export default connect(
  null,
  mapDispatchToProps
)(ArtistReleases)
