import _ from 'lodash'
import axios from 'axios'
import React from 'react'
import { Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { IoSearchOutline } from 'react-icons/io5'

import { setLoading } from '../../modules/counter'

import './journalList.css'

import { apiUrl, convertDate, mainUrl } from '../../utils'

export const DateAndAuthor = ({ entry }) => (
  <span>
    { entry.meta &&
      <p className="entry-date-author">
        {convertDate(entry.meta.first_published_at)}
        {entry.author && 
          <span>
            <span> - By </span> 
            <Link to={`/journal-author/${entry.author.slug}`}>
              {entry.author.name}
            </Link>
          </span>
        }
      </p>
    }
  </span>
)

class JournalListPage extends React.Component {
  constructor(props) {
    super(props)
    this.inputRef = React.createRef();
    this.state = {
      error: false,
      loading: false,
      journals: [],
      limit: 8,
      searchTerm: "",
    }
    window.onscroll = () => {
      const {
        loadJournals,
        state: { error, loading, hasMore }
      } = this
      if (error || loading || !hasMore) return
      const atBottomOfPage = window.innerHeight ? 
        (window.innerHeight + window.pageYOffset) >= document.body.offsetHeight - 2 :
        document.documentElement.scrollHeight - document.documentElement.scrollTop === document.documentElement.clientHeight
      if (atBottomOfPage) {
        // call some loading method
        loadJournals()
      }
    }
    this.handleInputChange = this.handleInputChange.bind(this)
    this.handleInputChangeDebounced = this.handleInputChangeDebounced.bind(this)
    this.handleInputClick = this.handleInputClick.bind(this)
  }

  componentDidMount () {
    this.loadJournals()
  }

  handleInputChange (e) {
    this.handleInputChangeDebounced(e.target.value)
  }

  handleInputChangeDebounced = _.debounce((searchTerm) => {
    this.setState({ searchTerm: searchTerm })
    this.loadJournals(true)    
  }, 300)

    handleInputClick (e) {
      e.target.select();
    }

  loadJournals = (searching = false) => {
    this.setState({ loading: true }, () => {
      const { journals, limit, searchTerm } = this.state
      const searchQuery = !!searchTerm ? `&search=${searchTerm}` : ""
      const offset = searching ? 0 : journals.length
      axios
        .get(
          `${apiUrl}/pages/?type=home.JournalPage&fields=_,id,title,slug,main_image,abstract,author,categories,detail_url,first_published_at,slug&remove_listing=false&limit=${limit}&offset=${offset}${searchQuery}&order=-first_published_at`
        )
        .then(res => {
          const newJournals = searching ? res.data.items : [...this.state.journals, ...res.data.items]
          const hasMore = newJournals.length < res.data.meta.total_count
          this.setState({
            hasMore,
            loading: false,
            journals: newJournals
          })
          this.props.setLoading(false)
        })
        .catch(err => {
          this.setState({
            error: err.message,
            loading: false
          })
        })
    })
  }

  render () {
    return (
      <div className="journal-container">
        <h1 className="search-headline">
          <span>Journal</span>
          <span className="headline-search">
            <input 
              ref={this.inputRef} 
              className={this.state.searchTerm && 'searching'} 
              onChange={this.handleInputChange}
              onClick={this.handleInputClick}
            />
            <IoSearchOutline />
          </span> 
        </h1>
        <ul className="entry-list">
          {this.state.journals.map( (entry, index) => (
            <li 
              className="entry-card"
              key={`entry_${index}`}
            >
              {/*<div className="entry-card-background">
                */}
                <div>
                  <Link 
                    to={`/journal/${entry.meta.slug}`}
                    className="entry-main-link"
                  >
                    <div className="entry-img-container">
                      <img src={`${mainUrl}${entry.main_image.url}`} alt={entry.title}/>
                    </div>
                    <h2 className="entry-title">{entry.title}</h2>
                  </Link>
                  <DateAndAuthor entry={entry} />
                  <p className="entry-abstract">{entry.abstract}</p>
                  <p className="entry-categories">
                    {entry.categories.map( (cat, i) => (
                    	<Link 
                    		to={`/categories/${cat.slug}`}
                        key={`entry_cat_${i}`}
                      >{cat.name}</Link> 
                    ))}
                  </p>
                {/*</div>
              */}
              </div>
            </li>
          ))}
        </ul>
      </div>
    )
  }
}

 const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLoading
    },
    dispatch
  )

export default connect(
  null,
  mapDispatchToProps
)(JournalListPage)

