import React from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { apiUrl, mainUrl, convertDate } from '../../utils'
import { setLoading } from '../../modules/counter'

import './authorList.css'

class JournalAuthorList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      JournalEntries: [],
    }
    this.pathnameToSlug = this.pathnameToSlug.bind(this);
    this.pathnameToName = this.pathnameToName.bind(this);
  }

  pathnameToSlug() {
    const pathnameArray = this.props.location.pathname.split('/').filter(Boolean)
    return pathnameArray[pathnameArray.length - 1]
  }

  pathnameToName() {
    const pathnameArray = this.props.location.pathname.split('/').filter(Boolean)
    return pathnameArray[pathnameArray.length - 1].replace('-', ' ')
  }

  componentDidMount () {
    // const catSlug = this.props.location.state.id
    const getURL = `${apiUrl}/journal/?author=${this.pathnameToSlug()}`
    axios.get(getURL).then(res => {
      console.log(res.data.items)
      this.setState({ JournalEntries: res.data.items })
      this.props.setLoading(false)
    })
  }

  render () {
    return (
      <div>
        <h1>
          <div className="h1-subtext">author</div>
          {this.pathnameToName()}
        </h1>
        
        <ul>
        {this.state.JournalEntries.map( (entry, index) => (
          <li 
            key={`entry_${index}`}
            className="author-list-entry"
          >
            <Link to={`/journal/${entry.meta.slug}`}>
              <div className="author-list-img-wrap">
                <img src={`${mainUrl}${entry.main_image.url}`} alt={entry.title} />
              </div>
              <div className="author-list-text">
                <h2>{entry.title}</h2>
                <p className="author-list-date">{convertDate(entry.meta.first_published_at)}</p>
                <p className="author-list-abstract">{entry.abstract}</p>
              </div>
            </Link>
          </li>
        ))}
        </ul>
      </div>
    )
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLoading
    },
    dispatch
  )

export default connect(
  null,
  mapDispatchToProps
)(JournalAuthorList)