import axios from 'axios'
import _ from 'lodash'
import React from 'react'
import { Link } from 'react-router-dom'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { apiUrl, mainUrl } from '../../utils'
import { DateAndAuthor } from './index'
import { ArtistTitleSplit } from '../releases/releaseListLink'
// import RelatedContent from '../related'
import CategoryWidget from '../categories/categoryWidget'

import { setLoading } from '../../modules/counter'

import './journalDetail.css'

const BackgroundImageContainer = ({ state }) => {
  const headerImageSrc = state.headerImage ? state.headerImage.url : ""
  const backgroundImageSrc = state.backgroundImage ? state.backgroundImage.url : headerImageSrc
  const focalPoint = state.backgroundImage ? 'backgroundImage' : 'headerImage'
  const backgroundHolder = !state.removeBackgroundImage ? `${state[focalPoint].left_split_point}% ${state[focalPoint].top_split_point}% / cover no-repeat url(${mainUrl}${backgroundImageSrc})` : '#333'
  return (
    <div 
      className="journal-detail-header-img"
      style={{'background': backgroundHolder}}
    ></div>
  )
}

class JournalDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      wholeEntry: {},
      title: "",
      headerImage: "",
      backgroundImage: "",
      removeBackgroundImage: false,
      titleHasBackground: false,
      titleHasSmallText: false,
      titleHasLargeText: false,
      content: [],
      prev: {},
      next: {},
      categories: [],
      relatedPages: [],
      currPageId: '',
      flippedHeader: false,
    }

    window.onscroll = (e) => {
      if (window.scrollY > (3 * (window.innerHeight/4) - 16)) {
        this.setState({flippedHeader: true})
      } else {
        this.setState({flippedHeader: false})
      }
    }
    this.loadContent = this.loadContent.bind(this);
    this.getSlugFromPathname = this.getSlugFromPathname.bind(this);
    this.getHeadlineClasses = this.getHeadlineClasses.bind(this);
  }

  loadContent (pageSlug) {
    // clear previous state variables
    this.setState({ content: [] })
    this.setState({ prev: {} })
    this.setState({ next: {} })

    // get main content
    const getURL = `${apiUrl}/pages/find/?html_path=${this.props.location.pathname}`
    axios.get(getURL).then(res => {
      this.setState({ wholeEntry: res.data})
      this.setState({ title: res.data.title })
      this.setState({ headerImage: res.data.main_image })
      this.setState({ backgroundImage: res.data.bkg_image })
      this.setState({ removeBackgroundImage: res.data.remove_background_image })
      this.setState({ titleHasBackground: res.data.title_background })
      this.setState({ titleHasSmallText: res.data.small_text })
      this.setState({ titleHasLargeText: res.data.large_text })
      this.setState({ content: res.data.content })
      this.setState({ categories: res.data.categories })
      this.setState({ currPageId: res.data.id})
    })

    // get prev and next entries
    axios.get(`${apiUrl}/pages/?type=home.JournalPage&fields=_,slug&remove_listing=false&order=-first_published_at`).then(res => {
      const slugArray = res.data.items.map(item => item.meta.slug)
      const entryIndex = slugArray.findIndex(slug => pageSlug === slug)
      const prevSlug = slugArray[entryIndex - 1]
      const nextSlug = slugArray[entryIndex + 1]
      setTimeout(() => {
        this.props.setLoading(false)
      }, 500)
      

      if (prevSlug) {
        axios.get(`${apiUrl}/pages/find/?html_path=/journal/${prevSlug}`).then(res => {
          this.setState({ prev: res.data })
        })
      } else {
        this.setState({ prev: {} })
      }

      if (nextSlug) {
        axios.get(`${apiUrl}/pages/find/?html_path=/journal/${nextSlug}`).then(res => {
          this.setState({ next: res.data })
        })
      } else {
        this.setState({ next: {} })
      }
    })
    // // scroll to top of page
    // window.scrollTo(0, 0)

  }

  getSlugFromPathname () {
    const pageSlugArray = this.props.location.pathname.split('/').filter(Boolean)
    return pageSlugArray[pageSlugArray.length - 1]
  }

  componentDidMount () {
    this.loadContent(this.getSlugFromPathname())
  }

  componentDidUpdate(prevProps) {
    if (this.props.location.pathname !== prevProps.location.pathname) {
      this.loadContent(this.getSlugFromPathname()) 
    }
  }

  getHeadlineClasses() {
    const ts = this.state
    let cssClassText = ''
    if (ts.titleHasBackground && ts.titleHasSmallText && ts.titleHasLargeText) {
      cssClassText = 'has-background has-small-text'
    } else if (!ts.titleHasBackground && ts.titleHasSmallText && ts.titleHasLargeText) {
      cssClassText = 'has-small-text'
    } else if (!ts.titleHasBackground && !ts.titleHasSmallText && ts.titleHasLargeText) {
      cssClassText = 'has-large-text'
    } else if (ts.titleHasBackground && !ts.titleHasSmallText && !ts.titleHasLargeText) {
      cssClassText = 'has-background'
    } else if (ts.titleHasBackground && !ts.titleHasSmallText && ts.titleHasLargeText) {
      cssClassText = 'has-background has-large-text'
    } else if (ts.titleHasBackground && ts.titleHasSmallText && !ts.titleHasLargeText) {
      cssClassText = 'has-background has-small-text'
    }
    return cssClassText
  }

  render () {
    return (
      <div className="journal-detail">
        <div className={this.state.flippedHeader ? 'journal-detail-header flipped' : 'journal-detail-header'}>
          <BackgroundImageContainer state={this.state} />
          <div className="headline-container">
            <h1 className={this.getHeadlineClasses()}>
              <ArtistTitleSplit title={this.state.title} />
            </h1>
          </div>
        </div>
        <div className="journal-detail-content">
          <h1 className={this.getHeadlineClasses()}>
            <ArtistTitleSplit title={this.state.title} />
          </h1>
          <div className="journal-entry">
            {/*<h2 className="journal-upper-title">{this.state.title}</h2>*/}
            {/*<img src={`${mainUrl}${this.state.headerImage.url}`} alt={this.state.title} />*/}
            {/*<h2 className="journal-lower-title">{this.state.title}</h2>*/}
            <DateAndAuthor entry={this.state.wholeEntry} />
            <ul>
              {this.state.content.map( (block, index) => (
                <li key={`block_${index}`}>
                  <div dangerouslySetInnerHTML={{ __html: block.value }} />
                </li>
              ))}
            </ul>
            
            <div className="journal-detail-prev-next">
              {!_.isEmpty(this.state.prev) ?
                <div className="journal-detail-prev">
                  <div>
                    <Link to={`/journal/${this.state.prev.meta.slug}`} >
                      <img src={`${mainUrl}${this.state.prev.main_image.url}`} alt={this.state.prev.title} />
                      <div>
                        <p>next</p>
                        <h4>{this.state.prev.title}</h4>
                      </div>
                    </Link>
                  </div>
                </div>
                : <span></span>
              }
              {!_.isEmpty(this.state.next) ?
                <div className="journal-detail-next">
                  <div>
                    <Link to={`/journal/${this.state.next.meta.slug}`} >
                      <div>
                        <p>previous</p>
                        <h4>{this.state.next.title}</h4>
                      </div>
                      <img src={`${mainUrl}${this.state.next.main_image.url}`} alt={this.state.next.title} />
                    </Link>
                  </div>
                </div>
                : <span></span>
              }
            </div>
            <CategoryWidget categories={this.state.categories} />


            {/*{ this.state.categories.length > 0 &&
              <div className="journal-detail-categories-container">
                <p className="journal-detail-categories-title">categories</p>
                <p className="journal-detail-categories">
                  {this.state.categories.map( (cat, i) => (
                    <Link 
                      to={`/categories/${cat.slug}`}
                      key={`entry_cat_${i}`}
                    >{cat.name}</Link> 
                  ))}
                </p>
              </div>
            }*/}
          </div>
        </div>
        {/*{this.state.currPageId &&
          <RelatedContent categories={this.state.categories} currPageId={this.state.currPageId} />
        }*/}
        {/*<div className="related-content">
          <ul>
            {this.state.relatedPages.map( (relatedPage, index) => (
              <Link to={relatedPage.url_path} key={`relatedPage_${index}`}>
                <li>
                  <p>{relatedPage.title}</p>
                  <p>{relatedPage.content_type__model}</p>
                  <img src={relatedPage.category_image__file} alt={relatedPage.title} />
                </li>
              </Link>
            ))}
          </ul>
        </div>*/}
      </div>
    )
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setLoading
    },
    dispatch
  )

export default connect(
  null,
  mapDispatchToProps
)(JournalDetail)
