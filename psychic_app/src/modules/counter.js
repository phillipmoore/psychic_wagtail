import _ from 'lodash' 
import {loadState, saveState} from '../localStorage'

export const INCREMENT_REQUESTED = 'counter/INCREMENT_REQUESTED'
export const INCREMENT = 'counter/INCREMENT'
export const DECREMENT_REQUESTED = 'counter/DECREMENT_REQUESTED'
export const DECREMENT = 'counter/DECREMENT'
export const ADDTOCART = 'counter/ADDTOCART'
export const REMOVEFROMCART = 'counter/REMOVEFROMCART'
export const ADDRELEASEURL = 'counter/ADDRELEASEURL'
export const REMOVERELEASEURL = 'counter/REMOVERELEASEURL'
export const SETIFRAMESRC = 'counter/SETIFRAMESRC'
export const REMOVEIFRAMESRC = 'counter/REMOVEIFRAMESRC'
export const SETLOADING = 'counter/SETLOADING'

const loadedState = loadState()
const initialState = loadedState ? loadedState : {
  count: 0,
  isIncrementing: false,
  isDecrementing: false,
  cart: [],
  releaseUrl: "",
  iFrameSrc: "",
  loading: false
}

export default (state = initialState, action) => {
  const cartCopy = _.cloneDeep(state.cart)

  switch (action.type) {
    case INCREMENT_REQUESTED:
      return {
        ...state,
        isIncrementing: true
      }

    case INCREMENT:
      return {
        ...state,
        count: state.count + 1,
        isIncrementing: !state.isIncrementing
      }

    case DECREMENT_REQUESTED:
      return {
        ...state,
        isDecrementing: true
      }

    case DECREMENT:
      return {
        ...state,
        count: state.count - 1,
        isDecrementing: !state.isDecrementing
      }

    case ADDTOCART:
      const addItemIndex = cartCopy.findIndex(c => c.id === action.item.id)

      if (addItemIndex !== -1) {
        if (cartCopy[addItemIndex].inventoryQuantity !== cartCopy[addItemIndex].quantity) {
          cartCopy[addItemIndex].quantity += 1
        }
      } else {
        cartCopy.push(action.item)
      }

      const addedState = {
        ...state,
        cart: cartCopy
      }
      saveState(addedState) // saving to local storage
      return addedState

    case REMOVEFROMCART:
      const removeItemIndex = cartCopy.findIndex(c => c.id === action.item.id)
      
      if (cartCopy[removeItemIndex].quantity > 1) {
        cartCopy[removeItemIndex].quantity -= 1
      } else {
        cartCopy.splice(removeItemIndex, 1)
      }
      const removedState = {
        ...state,
        cart: cartCopy
      }
      saveState(removedState) // saving to local storage
      return removedState

    case ADDRELEASEURL:
      return {
        ...state,
        releaseUrl: action.url
      }

    case REMOVERELEASEURL:
      return {
        ...state,
        releaseUrl: ""
      }

    case SETIFRAMESRC:
      return {
        ...state,
        iFrameSrc: action.iFrameSrc
      }

    case REMOVEIFRAMESRC:
      return {
        ...state,
        iFrameSrc: ""
      }

    case SETLOADING:
      return {
        ...state,
        loading: action.loading
      }

    default:
      return state
  }
}

export const increment = () => {
  return dispatch => {
    dispatch({
      type: INCREMENT_REQUESTED
    })

    dispatch({
      type: INCREMENT
    })
  }
}

export const incrementAsync = () => {
  return dispatch => {
    dispatch({
      type: INCREMENT_REQUESTED
    })

    return setTimeout(() => {
      dispatch({
        type: INCREMENT
      })
    }, 3000)
  }
}

export const decrement = () => {
  return dispatch => {
    dispatch({
      type: DECREMENT_REQUESTED
    })

    dispatch({
      type: DECREMENT
    })
  }
}

export const decrementAsync = () => {
  return dispatch => {
    dispatch({
      type: DECREMENT_REQUESTED
    })

    return setTimeout(() => {
      dispatch({
        type: DECREMENT
      })
    }, 3000)
  }
}

export const addToCart = (item) => {
  return dispatch => {
    dispatch({
      type: ADDTOCART,
      item: item
    })
  }
}

export const removeFromCart = (item) => {
  return dispatch => {
    dispatch({
      type: REMOVEFROMCART,
      item: item
    })
  }
}

export const addReleaseUrl = (url) => {
  return dispatch => {
    dispatch({
      type: ADDRELEASEURL,
      url: url
    })
  }
}

export const removeReleaseUrl = () => {
  return dispatch => {
    dispatch({
      type: ADDRELEASEURL
    })
  }
}

export const setIFrameSrc = (iFrameSrc) => {
  return dispatch => {
    dispatch({
      type: SETIFRAMESRC,
      iFrameSrc: iFrameSrc
    })
  }
}

export const removeIFrameSrc = () => {
  return dispatch => {
    dispatch({
      type: REMOVEIFRAMESRC
    })
  }
}

export const setLoading = (loading) => {
  return dispatch => {
    dispatch({
      type: SETLOADING,
      loading: loading
    })
  }
}
