export const mainUrl = process.env.NODE_ENV === 'development' ? 'http://localhost:8000' : 'https://psychic-hotline.net'

export const apiUrl = `${mainUrl}/api/v2`

export const nameToSlug = (name) => {
	return name.replace(/ /g,'-').toLowerCase()
}

export const convertDate = (dateString) => {
	const options = { year: 'numeric', month: 'long', day: 'numeric' };
	const date = new Date(dateString)
	return date.toLocaleDateString("en-US", options)
}

export const convertAmbientPrice = (int) => {
	return (int/100).toFixed(2)
}

export const convertApiPrice = (string) => {
	return parseFloat(string).toFixed(2)
}
