from django.db import models

from wagtail.admin.edit_handlers import FieldPanel
from wagtail.snippets.models import register_snippet
from wagtail.search import index


@register_snippet
class JournalCategory(models.Model, index.Indexed):
    """Journal category for a snippet."""

    name = models.CharField(max_length=255)
    slug = models.SlugField(
        verbose_name="slug",
        allow_unicode=True,
        max_length=255,
        help_text='A slug to identify posts by this category',
    )

    panels = [
        FieldPanel("name"),
        FieldPanel("slug"),
    ]

    search_fields = [
        index.SearchField('name'),
    ]

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"
        ordering = ["name"]

    def __str__(self):
        return self.name


@register_snippet
class Author(models.Model):
    """Author for a snippet."""

    name = models.CharField(max_length=255)
    slug = models.SlugField(
        verbose_name="slug",
        allow_unicode=True,
        max_length=255,
        help_text='A slug to identify authors',
    )

    panels = [
        FieldPanel("name"),
        FieldPanel("slug"),
    ]

    class Meta:
        verbose_name = "Author"
        verbose_name_plural = "Authors"
        ordering = ["name"]

    def __str__(self):
        return self.name

RELEASE_TYPE_CHOICES_DICT = {
    'PH': 'PH Releases',
    'OR': 'Other Releases',
    'SS': 'Singles Series',
    'SG': 'Soft Goods',
}

RELEASE_TYPE_CHOICES = [
    ('PH', 'PH Releases'),
    ('OR', 'Other Releases'),
    ('SS', 'Singles Series'),
    ('SG', 'Soft Goods'),
]

@register_snippet
class ReleaseType(models.Model):
    """Author for a snippet."""
    release_type = models.CharField(
        max_length=2,
        choices=RELEASE_TYPE_CHOICES,
        default='PH',
        unique=True,
    )

    class Meta:
        verbose_name = "Release Type"
        verbose_name_plural = "Release Types"

    def __str__(self):
        return RELEASE_TYPE_CHOICES_DICT[self.release_type]

