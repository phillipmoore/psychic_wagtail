from datetime import date
from urllib.request import urlopen
from bs4 import BeautifulSoup
import re
import json
import copy
from collections import OrderedDict

# from django import setup
from wagtail.core import blocks
from wagtail.images.blocks import ImageChooserBlock
from wagtail.images.models import SourceImageIOError
from wagtail.images.api.fields import ImageRenditionField
from rest_framework import serializers

# Blocks
class ExternalLinkBlock(blocks.StructBlock):
    text = blocks.CharBlock(required=True)
    url = blocks.URLBlock(
        required=True,
        help_text='Full url, include http(s)://...',
    )

    class Meta:
        icon = 'link'


class VideoBlock(blocks.StructBlock):
    code = blocks.CharBlock(
        required=True,
        help_text='iframe embed code',
    )

    class Meta:
        icon = 'video'


class ExternalLinkListBlock(blocks.StructBlock):
    external_link = blocks.ListBlock(ExternalLinkBlock())


class EmbedBlock(blocks.StructBlock):
    code = blocks.CharBlock(required=True)

    class Meta:
        icon = 'media'

    def get_api_representation(self, value, context=None):
        soup = BeautifulSoup(value['code'], features="html5lib")
        iframe = soup.find('iframe')

        if 'youtube' in iframe.attrs['src'] and iframe.attrs['width'] and iframe.attrs['height']:
            iframe.attrs.pop('width')
            iframe.attrs.pop('height')
            iframe.attrs['class'] = 'embed-youtube'


        if 'bandcamp' in iframe.attrs['src'] and 'size=small' in iframe.attrs['src']:
            new_div_container = soup.new_tag('div')
            new_div_container.attrs['class'] = 'embed-bandcamp-container'
            new_div = soup.new_tag('div')
            new_div.attrs['class'] = 'embed-bandcamp'
            new_div.append(iframe)

            iframe_copy = BeautifulSoup(str(iframe))
            helper_div = soup.new_tag('div')
            helper_div.attrs['class'] = 'embed-bandcamp-helper'
            helper_div.append(iframe_copy)
            
            new_div_container.append(new_div)
            new_div_container.append(helper_div)
            iframe = new_div_container

        elif 'bandcamp' in iframe.attrs['src'] and 'size=small'  not in iframe.attrs['src']:
            new_div_container = soup.new_tag('div')
            new_div_container.attrs['class'] = 'embed-bandcamp-center'
            new_div_container.append(iframe)
            iframe = new_div_container

        return str(iframe) if str(iframe) else value['code']


class AmbientDetailBlock(blocks.StructBlock):
    product_detail_url = blocks.URLBlock(
        required=True,
        help_text='add Ambient Ink product detail URL here',
    )

    def get_api_representation(self, value, context=None):
        try:
            html = urlopen(value['product_detail_url'])
        except:
            # products is defined and empty at ths point
            # TODO: make sure person inputting ambient handle knows this page does not exist
            pass
        else:
            soup = BeautifulSoup(html, features="html5lib")
            output = soup.title.text
            text_pattern = "var SPOParams"
            script_text = soup.find('script', string=re.compile(text_pattern), src=False)
            cleaned_script_text = re.sub('\n', '', re.sub('\t', '', script_text.text.split('var SPOParams = ')[1].split(';\n')[0]))
            json_output = json.loads(cleaned_script_text)

        return json_output


def get_image_rendition(context, image, image_filter, return_html=False):
    '''a function that takes context, a wagtail image and a filter (max-165x165) 
    returns an image rendition path. attempts are made to hyperlink'''
    # import pdb;pdb.set_trace()
    prefix = ''
    url_scheme = context['request'].META['wsgi.url_scheme']
    http_host = context['request'].META['HTTP_HOST']
    if url_scheme and http_host:
        prefix = "%s://%s" % (url_scheme, http_host)
    url = "%s%s" % (prefix, image.get_rendition(image_filter).url)

    return '<img src="%s" alt="%s" />' % (url, image.title) if return_html else url


class ReleaseBlock(blocks.PageChooserBlock):
    def get_api_representation(self, value, context=None): 
        path_list = [x for x in value.url_path.split('/') if x]
        image_path = get_image_rendition(context, value.featured_image, 'max-165x165', True) # use thumbnail
        release = value.title
        artist = value.artist.title
        today = date.today()
        action_text = "Buy" if value.release_date <= today else "Pre-Order"
        title_release_html = '<div class="title-release"><div>%s</div><div>%s</div></div>' % (artist, release)
        
        return_tuple = (
            path_list[-2], 
            path_list[-1], 
            title_release_html,
            image_path,
            title_release_html,
            action_text,
        )
        return '<div class="release-block"><a href="/%s/%s/">%s%s%s<div class="buy">%s</div></a></div>' % return_tuple

class HomeReleaseBlock(blocks.PageChooserBlock):
    def get_api_representation(self, value, context=None):
        # import pdb; pdb.set_trace()
        release = None
        if value:
            artist_title = value.artist.title if value.artist else value.alt_artists
            release = {
                'artist_title': artist_title,
                'release_title': value.title,
                'slug': value.slug,
                'link_text': "Buy" if value.release_date <= date.today() else "Pre-Order"
            }
        return release


class APIImageChooserBlock(ImageChooserBlock):
    ''' 
        takes "return_html" parameter (not required). returns image path or <img/> with src.
        TODO: pass in filter parameter if we need diferent sized images.
     '''
    def get_api_representation(self, value, context=None):
        return_html = 'return_html' in self.meta.__dict__ and self.meta.return_html
        return get_image_rendition(context, value, 'max-800x600', return_html)
        # return ImageSerializer(context=context).to_representation(value)

def get_focal_point(value):
    top_split_point = 50
    left_split_point = 50
    if value.focal_point_y:
        top_split_point = round((((value.focal_point_y)/value.height)) * 100, 2)
    if value.focal_point_x:
        left_split_point = round((((value.focal_point_x)/value.width)) * 100, 2)
    return {
        'top_split_point': top_split_point,
        'left_split_point': left_split_point,
    }

class APIHomeImageChooserBlock(ImageChooserBlock):
    def get_api_representation(self, value, context=None):
        focal_point = get_focal_point(value)
        return {
            'src': get_image_rendition(context, value, 'max-2000x2000'),
            'top_split_point': focal_point['top_split_point'],
            'left_split_point': focal_point['left_split_point'],
        }

class HomeImageBlock(blocks.StructBlock):
    image = APIHomeImageChooserBlock()
    release = HomeReleaseBlock(required=False, target_model='home.ReleasePage')
    title_background = blocks.BooleanBlock(required=False)


class JournalImageRenditionField(ImageRenditionField):
    def to_representation(self, image):
        try:
            thumbnail = image.get_rendition(self.filter_spec)
            focal_point = get_focal_point(image)
            return OrderedDict([
                ('url', thumbnail.url),
                ('width', thumbnail.width),
                ('height', thumbnail.height),
                ('alt', thumbnail.alt),
                ('top_split_point', focal_point['top_split_point']),
                ('left_split_point', focal_point['left_split_point']),
            ])
        except SourceImageIOError:
            return OrderedDict([
                ('error', 'SourceImageIOError'),
            ])

class ImageAndCaptionBlock(blocks.StructBlock):
    image = APIImageChooserBlock(required=True)
    caption = blocks.CharBlock(required=False)

    def get_api_representation(self, value, context=None):
        image_html = get_image_rendition(context, value['image'], 'max-1000x1000', True)
        caption = '<div class="caption">%s</div>' % value['caption'] if value['caption'] else ''
        return '<div class="image-and-caption"><div class="image">%s</div>%s</div>' % (image_html, caption)

    class Meta:
        icon = 'image'
