# Generated by Django 3.2.5 on 2021-07-18 20:11

import datetime
from django.db import migrations, models
import django.db.models.deletion
import wagtail.core.blocks
import wagtail.core.fields
import wagtail.images.blocks


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailimages', '0023_add_choose_permissions'),
        ('home', '0003_auto_20210718_1729'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='artistpage',
            name='header_image',
        ),
        migrations.AddField(
            model_name='artistpage',
            name='images',
            field=wagtail.core.fields.StreamField([('image', wagtail.images.blocks.ImageChooserBlock())], blank=True),
        ),
        migrations.AddField(
            model_name='journalpage',
            name='background_image',
            field=models.ForeignKey(blank=True, help_text='leave blank to use Header Image', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailimages.image'),
        ),
        migrations.AddField(
            model_name='journalpage',
            name='remove_background_image',
            field=models.BooleanField(default=False, help_text='select this option if you do not wish to use a background image on this journal entry'),
        ),
        migrations.AddField(
            model_name='releasepage',
            name='release_date',
            field=models.DateField(default=datetime.date.today, verbose_name='Release date'),
        ),
        migrations.AlterField(
            model_name='artistpage',
            name='links',
            field=wagtail.core.fields.StreamField([('external_link', wagtail.core.blocks.StructBlock([('text', wagtail.core.blocks.CharBlock(required=True)), ('url', wagtail.core.blocks.URLBlock(help_text='Full url, include http(s)://...', required=True))]))], blank=True),
        ),
        migrations.AlterField(
            model_name='journalpage',
            name='content',
            field=wagtail.core.fields.StreamField([('text_content', wagtail.core.blocks.RichTextBlock(features=['h2', 'h3', 'h4', 'bold', 'italic', 'link', 'ol', 'ul', 'hr', 'link', 'image'])), ('full_page_link', wagtail.core.blocks.StructBlock([('text', wagtail.core.blocks.CharBlock(required=True)), ('url', wagtail.core.blocks.URLBlock(help_text='Full url, include http(s)://...', required=True))])), ('embed_block', wagtail.core.blocks.StructBlock([('code', wagtail.core.blocks.CharBlock(required=True))])), ('image', wagtail.images.blocks.ImageChooserBlock()), ('release_block', wagtail.core.blocks.PageChooserBlock(page_type=['home.ReleasePage'], required=False))], blank=True),
        ),
        migrations.AlterField(
            model_name='releasepage',
            name='content',
            field=wagtail.core.fields.StreamField([('text_content', wagtail.core.blocks.RichTextBlock(features=['h2', 'h3', 'h4', 'bold', 'italic', 'link', 'ol', 'ul', 'hr', 'link', 'image'])), ('full_page_link', wagtail.core.blocks.StructBlock([('text', wagtail.core.blocks.CharBlock(required=True)), ('url', wagtail.core.blocks.URLBlock(help_text='Full url, include http(s)://...', required=True))])), ('embed_block', wagtail.core.blocks.StructBlock([('code', wagtail.core.blocks.CharBlock(required=True))])), ('image', wagtail.images.blocks.ImageChooserBlock())], blank=True, verbose_name='Synopsis'),
        ),
        migrations.AlterField(
            model_name='releasepage',
            name='second_content',
            field=wagtail.core.fields.StreamField([('text_content', wagtail.core.blocks.RichTextBlock(features=['h2', 'h3', 'h4', 'bold', 'italic', 'link', 'ol', 'ul', 'hr', 'link', 'image'])), ('full_page_link', wagtail.core.blocks.StructBlock([('text', wagtail.core.blocks.CharBlock(required=True)), ('url', wagtail.core.blocks.URLBlock(help_text='Full url, include http(s)://...', required=True))])), ('embed_block', wagtail.core.blocks.StructBlock([('code', wagtail.core.blocks.CharBlock(required=True))])), ('image', wagtail.images.blocks.ImageChooserBlock())], blank=True, verbose_name='One sheet'),
        ),
        migrations.AlterField(
            model_name='releasepage',
            name='third_content',
            field=wagtail.core.fields.StreamField([('text_content', wagtail.core.blocks.RichTextBlock(features=['h2', 'h3', 'h4', 'bold', 'italic', 'link', 'ol', 'ul', 'hr', 'link', 'image'])), ('full_page_link', wagtail.core.blocks.StructBlock([('text', wagtail.core.blocks.CharBlock(required=True)), ('url', wagtail.core.blocks.URLBlock(help_text='Full url, include http(s)://...', required=True))])), ('embed_block', wagtail.core.blocks.StructBlock([('code', wagtail.core.blocks.CharBlock(required=True))])), ('image', wagtail.images.blocks.ImageChooserBlock())], blank=True, verbose_name='Track list'),
        ),
    ]
