# Generated by Django 3.2.5 on 2021-07-24 22:15

from django.db import migrations, models
import django.db.models.deletion
import modelcluster.fields


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailimages', '0023_add_choose_permissions'),
        ('home', '0006_auto_20210724_2032'),
    ]

    operations = [
        migrations.AddField(
            model_name='artistpage',
            name='categories',
            field=modelcluster.fields.ParentalManyToManyField(blank=True, help_text='see "Snippets" in side panel', to='home.JournalCategory'),
        ),
        migrations.AddField(
            model_name='artistpage',
            name='category_image',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailimages.image'),
        ),
        migrations.AddField(
            model_name='journalpage',
            name='category_image',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailimages.image'),
        ),
        migrations.AddField(
            model_name='releasepage',
            name='categories',
            field=modelcluster.fields.ParentalManyToManyField(blank=True, help_text='see "Snippets" in side panel', to='home.JournalCategory'),
        ),
        migrations.AddField(
            model_name='releasepage',
            name='category_image',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailimages.image'),
        ),
    ]
