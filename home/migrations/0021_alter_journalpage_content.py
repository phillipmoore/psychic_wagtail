# Generated by Django 3.2.6 on 2021-08-23 17:36

from django.db import migrations
import home.blocks
import wagtail.core.blocks
import wagtail.core.fields

class Migration(migrations.Migration):

    dependencies = [
        ('home', '0020_auto_20210820_2232'),
    ]

    operations = [
        migrations.AlterField(
            model_name='journalpage',
            name='content',
            field=wagtail.core.fields.StreamField([('text_content', wagtail.core.blocks.RichTextBlock(features=['h2', 'h3', 'h4', 'bold', 'italic', 'ol', 'ul', 'hr', 'image', 'mylink'])), ('full_page_link', wagtail.core.blocks.StructBlock([('text', wagtail.core.blocks.CharBlock(required=True)), ('url', wagtail.core.blocks.URLBlock(help_text='Full url, include http(s)://...', required=True))])), ('embed_block', wagtail.core.blocks.StructBlock([('code', wagtail.core.blocks.CharBlock(required=True))])), ('image', home.blocks.APIImageChooserBlock(return_html=True)), ('image_and_caption', wagtail.core.blocks.StructBlock([('image', home.blocks.APIImageChooserBlock(required=True)), ('caption', wagtail.core.blocks.CharBlock(required=False))])), ('release_block', home.blocks.ReleaseBlock(page_type=['home.ReleasePage'], required=False))], blank=True),
        ),
    ]
