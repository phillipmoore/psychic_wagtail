from wagtail.core import hooks
from wagtail.core.whitelist import check_url
from draftjs_exporter.dom import DOM
from wagtail.admin.rich_text.converters.html_to_contentstate import PageLinkElementHandler, ExternalLinkElementHandler
import wagtail.admin.rich_text.editors.draftail.features as draftail_features

def link_entity(props):
    """
    <a linktype="page" id="1" url="...">internal page link</a>
    """
    
    id_ = props.get('id')
    link_props = {}

    if id_ is not None:
        link_props['linktype'] = 'page'
        link_props['id'] = id_
        link_props['href'] = check_url(props.get('url'))
    else:
        link_props['href'] = check_url(props.get('url'))

    return DOM.create_element('a', link_props, props['children'])


@hooks.register('register_rich_text_features')
def register_link_feature(features):
    features.default_features.append('mylink')

    feature_name = 'mylink'
    type_ = 'LINK'

    features.register_editor_plugin(
        'draftail', feature_name, draftail_features.EntityFeature({
            'type': type_,
            'icon': 'link',
            'description': 'MyLink',
            # We want to enforce constraints on which links can be pasted into rich text.
            # Keep only the attributes Wagtail needs.
            'attributes': ['url', 'id', 'parentId'],
            'whitelist': {
                # Keep pasted links with http/https protocol, and not-pasted links (href = undefined).
                'href': "^(http:|https:|undefined$)",
            }
        }, js=[
            'wagtailadmin/js/page-chooser-modal.js',
        ])
    )
    features.register_converter_rule('contentstate', feature_name, {
        'from_database_format': {
            'a[href]': ExternalLinkElementHandler(type_),
            'a[linktype="page"]': PageLinkElementHandler(type_),
        },
        'to_database_format': {
            'entity_decorators': {type_: link_entity}
        }
    })
