from rest_framework import serializers
from wagtail.images.models import Image
from .snippets import Author, JournalCategory, ReleaseType
# from .models import ArtistPage


# if we think we need to optimize for image size, switch to this
# from wagtail.images.api.fields import ImageRenditionField
class ImageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Image
        fields = ['title', 'file']

class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ['id', 'name', 'slug']

class JournalCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = JournalCategory
        fields = ['id', 'name', 'slug']

class ReleaseTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReleaseType
        fields = ['id', 'release_type']

# save for v2 odf site 
# class ArtistPageSerializer(serializers.ModelSerializer):
# 	class Meta:
# 		model = ArtistPage
# 		fields = ['id', 'title', 'type', 'detail_url']