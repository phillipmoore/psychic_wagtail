from datetime import date

from django.db import models
from django import forms
from django.utils.text import slugify
from django.contrib.postgres.fields import ArrayField

from rest_framework import serializers
from modelcluster.fields import ParentalManyToManyField

from wagtail.core import blocks
from wagtail.core.models import Page
from wagtail.core.fields import RichTextField, StreamField
from wagtail.admin.edit_handlers import (
        FieldPanel, 
        MultiFieldPanel, 
        InlinePanel, 
        StreamFieldPanel, 
        PageChooserPanel,
        FieldRowPanel,
    )
from wagtail.snippets.edit_handlers import SnippetChooserPanel
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.images.api.fields import ImageRenditionField
from wagtail.api import APIField
from wagtail.search import index

from .serializers import (
    ImageSerializer, 
    AuthorSerializer, 
    JournalCategorySerializer,
    ReleaseTypeSerializer,
)
from .blocks import (
    AmbientDetailBlock,
    APIImageChooserBlock,
    EmbedBlock,
    ExternalLinkBlock, 
    HomeImageBlock,
    ImageAndCaptionBlock,
    JournalImageRenditionField,
    ReleaseBlock,
    VideoBlock,
)


# you cannot include 'link' when using 'mylink' or the javascript breaks
DEFAULT_RTE_FEATURES = ['h2', 'h3', 'h4', 'bold', 'italic', 'ol', 'ul', 'hr', 'image','mylink']

# PsychicPage, a page mixin used to expose categories.
class PsychicPage(Page):
    class Meta:
        abstract = True

    categories = ParentalManyToManyField("home.JournalCategory", blank=True, help_text="see \"Snippets\" in side panel")
    category_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    display_on_home_page = models.BooleanField(
        default=False,
        help_text="Select this option if you want this page listed on the home page"
    )

    api_fields = [
        APIField('category_image'),
    ]


# Pages
class HomePage(Page):
    subpage_types = [
        'home.ArtistListPage',
        'home.JournalListPage',
        'home.ReleaseListPage',
        'home.VideoListPage',
    ]

    about_text = RichTextField(blank=True, features=DEFAULT_RTE_FEATURES)
    images = StreamField([
        ('image_block', HomeImageBlock())
    ], blank=True)

    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                StreamFieldPanel('images'),
            ],
            heading="Images",
            classname="collapsible"
        ),
        FieldPanel('about_text'),
    ]
    api_fields = [
        APIField('about_text'),
        APIField('images'),
    ]

# Artist Section
class ArtistListPage(Page):
    subpage_types = [
        'home.ArtistPage',
    ]

class ArtistPage(PsychicPage):
    bio = RichTextField(blank=True, features=DEFAULT_RTE_FEATURES)
    images = StreamField([
        ('image', APIImageChooserBlock())
    ], blank=True)
    links = StreamField([
        ('external_link', ExternalLinkBlock())
    ], blank=True)
    videos = StreamField([
        ('videos', VideoBlock())
    ], blank=True)
    bandsintown = models.CharField(max_length=256, blank=True)

    content_panels = Page.content_panels + [
        MultiFieldPanel(
            [
                StreamFieldPanel('images'),
            ],
            heading="Images",
            classname="collapsible"
        ),
        MultiFieldPanel(
            [
                FieldPanel('bio'),
            ],
            heading="Bio",
            classname="collapsible collapsed"
        ),
        MultiFieldPanel(
            [
                StreamFieldPanel('links'),
            ],
            heading="External Links",
            classname="collapsible collapsed"
        ),
        MultiFieldPanel(
            [
                StreamFieldPanel('videos'),
            ],
            heading="Youtube Embed Links",
            classname="collapsible collapsed"
        ),
        MultiFieldPanel(
            [
                FieldPanel('bandsintown'),
            ],
            heading="Bands in Town",
            classname="collapsible collapsed"
        ),
        MultiFieldPanel(
            [
                FieldPanel("categories", widget=forms.CheckboxSelectMultiple),
            ],
            heading="Categories",
            classname="collapsible collapsed"
        ),
        ImageChooserPanel('category_image'),
    ]

    api_fields = [
        APIField('bio'),
        APIField('images'),
        APIField('links'),
        APIField('videos'),
        APIField('bandsintown'),
        APIField('categories', serializer=JournalCategorySerializer(many=True)),
        APIField('related_image', serializer=ImageRenditionField('fill-800x800', source='category_image'))
    ]

    search_fields = Page.search_fields + [
        index.FilterField('categories'),
    ]

    def clean(self, *args, **kwargs):
        super().clean()
        # saving the category image for artists
        bb = self.images._bound_blocks
        if len(bb) > 0:
            if bb[0]:
                self.category_image = bb[0].value
        else:
            self.category_image = None



# Journal Section
class JournalListPage(Page):
    subpage_types = [
        'home.JournalPage',
    ]


class JournalPage(PsychicPage):
    header_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+'
    )
    background_image = models.ForeignKey(
        'wagtailimages.Image',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name='+',
        help_text="Leave blank to use header image as background image"
    )
    remove_background_image = models.BooleanField(
        default=False,
        help_text="Select this option if you do not wish to use a background image on this journal entry"
    )
    title_background = models.BooleanField(
        default=False,
        help_text="Select this option if you want a background on your title"
    )
    small_text = models.BooleanField(
        default=False,
        help_text="Select for long title"
    )
    large_text = models.BooleanField(
        default=False,
        help_text="Select for short title with short words"
    )
    content = StreamField([
        ('text_content', blocks.RichTextBlock(features=DEFAULT_RTE_FEATURES)),
        ('full_page_link', ExternalLinkBlock()),
        ('embed_block', EmbedBlock()),
        ('image', APIImageChooserBlock(return_html=True)),
        ('image_and_caption', ImageAndCaptionBlock()),
        ('release_block', ReleaseBlock(required=False, target_model='home.ReleasePage'))
    ], blank=True)
    abstract = models.CharField(max_length=350, blank=True)
    author = models.ForeignKey(
        "home.Author",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        help_text="Totally optional"
    )
    remove_listing = models.BooleanField(default=True, help_text="Uncheck this box for the whole world to see this jounal entry.")

    content_panels = Page.content_panels + [
        FieldPanel('remove_listing'),
        ImageChooserPanel('header_image'),
        FieldRowPanel(
            [
                ImageChooserPanel('background_image'),
                FieldPanel('remove_background_image'),
            ],
            heading="Background Image",
            classname=""
        ),
        FieldRowPanel(
            [
                FieldPanel('title_background'),
                FieldPanel('small_text'),
                FieldPanel('large_text'),
            ],
            heading="Title Options",
            classname=""
        ),
        MultiFieldPanel(
            [
                SnippetChooserPanel("author"),
            ],
            heading="Author",
            classname="collapsible collapsed"
        ),
        MultiFieldPanel(
            [
                FieldPanel('abstract'),
            ],
            heading="Abstract",
            classname="collapsible collapsed"
        ),
        MultiFieldPanel(
            [
                StreamFieldPanel('content'),
            ],
            heading="Content",
            classname="collapsible"
        ),
        MultiFieldPanel(
            [
                FieldPanel("categories", widget=forms.CheckboxSelectMultiple),
            ],
            heading="Categories",
            classname="collapsible collapsed"
        ),
        FieldPanel('display_on_home_page'),
    ]

    api_fields = [
        APIField('main_image', serializer=JournalImageRenditionField('max-2000x2000', source='header_image')),
        APIField('thumbnail_image', serializer=ImageRenditionField('fill-300x300', source='header_image')),
        APIField('bkg_image', serializer=JournalImageRenditionField('max-2000x2000', source='background_image')),
        APIField('remove_background_image'),
        APIField('title_background'),
        APIField('small_text'),
        APIField('large_text'),
        APIField('abstract'),
        APIField('author', serializer=AuthorSerializer()),
        APIField('content'),
        APIField('categories', serializer=JournalCategorySerializer(many=True)),
        APIField('related_image', serializer=ImageRenditionField('max-800x800', source='category_image')),
        APIField('remove_listing'),
        APIField('display_on_home_page'),
    ]

    search_fields = Page.search_fields + [
        index.SearchField('abstract'),
        index.SearchField('content'),
        index.FilterField('categories'),
        index.FilterField('remove_listing'),
    ]

    def clean(self, *args, **kwargs):
        super().clean()
        self.category_image = self.header_image


# Release Section
class ReleaseListPage(Page):
    subpage_types = [
        'home.ReleasePage',
    ]


RELEASE_TYPE_CHOICES = [
    ('PH', 'PH Releases'),
    ('OR', 'Other Releases'),
    ('SS', 'Singles Series'),
    ('SG', 'Soft Goods'),
]

class ArtistPageSerializer(serializers.ModelSerializer):
    category_image = ImageSerializer()
    class Meta:
        model = ArtistPage
        fields = ['id', 'title', 'category_image']

class ReleasePage(PsychicPage):
    artist = models.ForeignKey(
        'home.ArtistPage',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )
    alt_artists = models.CharField(
        max_length=250,
        blank=True,
        help_text="if no artist is selected for this release, use this field to add artists"
    )
    featured_image = models.ForeignKey(
        'wagtailimages.Image',
        null=False,
        blank=False,
        on_delete=models.PROTECT,
        related_name='+'
    )
    same_first_image = models.BooleanField(
        default=False,
        help_text="if the featured image is the same as the first ambient image, mark this field as true"
    )
    release_types = ParentalManyToManyField(
        "home.ReleaseType", 
        blank=True,
        related_name="release_types", 
        help_text="see \"Snippets\" in side panel",
    )
    release_type = models.CharField(
        max_length=2,
        choices=RELEASE_TYPE_CHOICES,
        default='PH',
    )
    release_date = models.DateField("Release date", default=date.today)
    content = StreamField([
        ('text_content', blocks.RichTextBlock(features=DEFAULT_RTE_FEATURES)),
        ('full_page_link', ExternalLinkBlock()),
        ('embed_block', EmbedBlock()),
        ('image', APIImageChooserBlock(return_html=True)),
    ], blank=True, verbose_name="Synopsis")
    second_content = StreamField([
        ('text_content', blocks.RichTextBlock(features=DEFAULT_RTE_FEATURES)),
        ('full_page_link', ExternalLinkBlock()),
        ('embed_block', EmbedBlock()),
        ('image', APIImageChooserBlock(return_html=True)),
    ], blank=True, verbose_name="One sheet")
    third_content = StreamField([
        ('text_content', blocks.RichTextBlock(features=DEFAULT_RTE_FEATURES)),
        ('full_page_link', ExternalLinkBlock()),
        ('embed_block', EmbedBlock()),
        ('image', APIImageChooserBlock(return_html=True)),
    ], blank=True, verbose_name="Track list")
    ambient_blocks = StreamField([
        ('ambient_block', AmbientDetailBlock())
    ], blank=True)
    searchable_artist_title = models.CharField(max_length=250, blank=True) 
    remove_listing = models.BooleanField(default=True, help_text="Uncheck this box for the whole world to see this release.")

    content_panels = Page.content_panels + [
        FieldPanel('remove_listing'),
        PageChooserPanel('artist'),
        FieldPanel('alt_artists'),
        FieldRowPanel(
            [
                ImageChooserPanel('featured_image'),
                FieldPanel('same_first_image'),
            ],
            heading="Image",
            classname=""
        ),
        
        FieldPanel('release_types', widget=forms.CheckboxSelectMultiple),
        FieldPanel('release_date'),
        MultiFieldPanel(
            [
                StreamFieldPanel('content'),
                StreamFieldPanel('second_content'),
                StreamFieldPanel('third_content'),
            ],
            heading="content",
            classname="collapsible collapsed"
        ),
        MultiFieldPanel(
            [
                StreamFieldPanel('ambient_blocks'),
            ],
            heading="Ambient Blocks",
            classname="collapsible collapsed"
        ),
        MultiFieldPanel(
            [
                FieldPanel("categories", widget=forms.CheckboxSelectMultiple),
            ],
            heading="Categories",
            classname="collapsible collapsed"
        ),
        FieldPanel('display_on_home_page'),
    ]

    api_fields = [
        APIField('alt_artists'),
        APIField('artist', serializer=ArtistPageSerializer()),
        APIField('release_image', serializer=ImageRenditionField('fill-800x800', source='featured_image')),
        APIField('same_first_image'),
        APIField('release_types', serializer=ReleaseTypeSerializer(many=True)),
        APIField('release_date'),
        APIField('content'),
        APIField('second_content'),
        APIField('third_content'),
        APIField('ambient_blocks'),
        APIField('categories', serializer=JournalCategorySerializer(many=True)),
        APIField('related_image', serializer=ImageRenditionField('fill-800x800', source='category_image')),
        APIField('remove_listing'),
        APIField('display_on_home_page'),
    ]

    search_fields = Page.search_fields + [
        index.FilterField('release_types', partial_match=True),
        index.SearchField('alt_artists'),
        index.SearchField('searchable_artist_title'),
        index.FilterField('categories'),
        index.FilterField('remove_listing'),
    ]

    def get_admin_display_title(self):
        artist_text = self.artist if self.artist else self.alt_artists
        return "%s - %s" % (artist_text, self.title)

    def clean(self):
        """Override the slug before saving."""
        super().clean()
        artist_text = self.artist if self.artist else self.alt_artists
        self.slug = slugify("%s %s" % (artist_text, self.title))

        if self.featured_image:
            # setting category image
            self.category_image = self.featured_image

        if self.artist:
            # setting searchable_artist_title, a way to search the artist__tile without elasticsearch 
            self.searchable_artist_title = self.artist.title

# Videos Section
class VideoListPage(Page):
    subpage_types = [
        'home.VideoPage',
    ]

class VideoPage(PsychicPage):
    artist = models.ForeignKey(
        'home.ArtistPage',
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
    )
    alt_artists = models.CharField(
        max_length=250,
        blank=True,
        help_text="if no artist is selected for this release, use this field to add artists",
    )
    featured_image = models.ForeignKey(
        'wagtailimages.Image',
        null=False,
        blank=False,
        on_delete=models.PROTECT,
        related_name='+',
    )
    embed_code = models.CharField(
        max_length=250,
        blank=True,
        verbose_name="iframe url",
        help_text="Make sure iframe url includes the word 'embed'"
    )

    content_panels = Page.content_panels + [
        PageChooserPanel('artist'),
        FieldPanel('alt_artists'),
        ImageChooserPanel('featured_image'),
        FieldPanel('embed_code'),
        MultiFieldPanel(
            [
                FieldPanel("categories", widget=forms.CheckboxSelectMultiple),
            ],
            heading="Categories",
            classname="collapsible"
        ),
        FieldPanel('display_on_home_page'),
    ]

    api_fields = [
        APIField('alt_artists'),
        APIField('artist', serializer=ArtistPageSerializer()),
        APIField('video_image', serializer=ImageRenditionField('fill-800x450', source='featured_image')),
        APIField('embed_code'),
        APIField('categories', serializer=JournalCategorySerializer(many=True)),
        APIField('related_image', serializer=ImageRenditionField('fill-800x450', source='category_image')),
        APIField('display_on_home_page'),
    ]

    search_fields = Page.search_fields + [
        index.FilterField('categories'),
    ]

    def clean(self):
        """Override the slug before saving."""
        super().clean()
        artist_text = self.artist if self.artist else self.alt_artists
        self.slug = slugify("%s %s" % (artist_text, self.title))

        if self.featured_image:
            # setting category image
            self.category_image = self.featured_image

        if self.artist:
            # setting searchable_artist_title, a way to search the artist__tile without elasticsearch 
            self.searchable_artist_title = self.artist.title
