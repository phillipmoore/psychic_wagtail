import os
from .base import *
from decouple import config

DEBUG = False

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = config('SECRET_KEY')

ALLOWED_HOSTS = [
	'psychic-hotline.net',
	'www.psychic-hotline.net', 
	'www.youtube.com', 
	'widget.bandsintown.com', 
	'bandcamp.com'
] 

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

try:
    from .local import *
except ImportError:
    pass
