from rest_framework import serializers

class PsychicPagesSerializer(serializers.Serializer):
    id = serializers.CharField(max_length=255)
    title = serializers.CharField(max_length=255)
    category_image__file = serializers.CharField(max_length=255)
    url_path = serializers.CharField(max_length=255)
    content_type__model = serializers.CharField(max_length=255)

    def to_representation(self, instance):
        # import pdb;pdb.set_trace()
        data = super(PsychicPagesSerializer, self).to_representation(instance)
        
        # make link path functional on front end
        url_parts = [x for x in data['url_path'].split('/') if x]
        url_parts.pop(0)
        url = '/%s/' % '/'.join(url_parts)
        data['url_path'] = url

        # build full image path
        if data['category_image__file']:
            host = self.context['request'].META['HTTP_HOST']
            url_scheme = self.context['request'].META['wsgi.url_scheme']
            full_image_path = '%s://%s/media/%s' % (url_scheme, host, data['category_image__file'])
            data['category_image__file'] = full_image_path

        return data
