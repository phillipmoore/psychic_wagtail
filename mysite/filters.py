from rest_framework.filters import BaseFilterBackend
from home.models import JournalPage, ReleasePage
from home.snippets import JournalCategory


class RemoveListingFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        remove_listing = request.query_params.get('remove_listing', None)
        if remove_listing is not None:
            boolean_rl = True if remove_listing == 'true' else False 
            queryset = queryset.filter(remove_listing=boolean_rl)

        return queryset.order_by('path')

class ReleaseTypeFilter(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        release_types = request.query_params.get('release_types', None)
        if release_types:
            rt_list = release_types.split(',')
            queryset = queryset.filter(release_types__release_type__in=rt_list).distinct()

        return queryset

class CategoryFilter(BaseFilterBackend):
    ''' allow filtering on categories cross page types for pages endpoint '''
    def filter_queryset(self, request, queryset, view):
        category = request.query_params.get('categories', None)
        if 'psychicpages' in request.META['PATH_INFO']:
            if category:
                page_type_sets = ['artistpage_set', 'journalpage_set', 'releasepage_set', 'videopage_set']
                # this try/except will determine if we are being given an integer or a slug representing a category
                try:
                    category = int(category)
                except ValueError:
                    category_obj = JournalCategory.objects.filter(slug=category).first()
                else:
                    category_obj = JournalCategory.objects.filter(id=category).first()

                if category_obj:
                    ids_list = []
                    for page_set in page_type_sets:
                        id_queryset = category_obj.__getattribute__(page_set).values_list('id', flat=True)
                        ids_list = ids_list + [x for x in id_queryset]

                    queryset = queryset.filter(pk__in=ids_list)
                else:
                    queryset = queryset.none()

        elif 'journal' in request.META['PATH_INFO']:
            if category:
                queryset = queryset.filter(categories__slug=category)

        return queryset

class AuthorFilter(BaseFilterBackend):
    ''' Change filter to use slug instead of id'''
    def filter_queryset(self, request, queryset, view):
        author = request.query_params.get('author', None)
        # import pdb;pdb.set_trace()
        if author:
            queryset = queryset.filter(author__slug=author)
        return queryset