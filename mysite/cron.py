import sys
from django.core.management import call_command

def my_backup():
	try:
		call_command('dbbackup', '--clean')
	except Exception as ex:
		print(sys.exc_info())

	try:
		call_command('mediabackup', '--clean')
	except Exception as ex:
		print(sys.exc_info())
