from django.shortcuts import render


from wagtail.api.v2.views import BaseAPIViewSet, PagesAPIViewSet
from home.models import JournalPage, PsychicPage, ReleasePage
from wagtail.core.models import Page
from .filters import AuthorFilter, CategoryFilter, ReleaseTypeFilter, RemoveListingFilter
from .serializers import PsychicPagesSerializer
from wagtail.api.v2.filters import ( ChildOfFilter, DescendantOfFilter, FieldsFilter, LocaleFilter, OrderingFilter,
    SearchFilter, TranslationOfFilter)


# point to react app index.html
def index(request):
    return render(request, "build/index.html")

# add AuthorFilter to JournalAPIViewSet so I can filter by 'author__slug'
class JournalAPIViewSet(BaseAPIViewSet):
    model = JournalPage
    BaseAPIViewSet.body_fields.append('title')
    BaseAPIViewSet.meta_fields = BaseAPIViewSet.meta_fields + [
        'slug',
        'first_published_at',
    ]
    BaseAPIViewSet.listing_default_fields = BaseAPIViewSet.listing_default_fields + [
        'title',
        'slug',
        'first_published_at',
        'main_image',
        'abstract',
        'author',
        'categories',
    ]
    BaseAPIViewSet.filter_backends.append(AuthorFilter)

class ReleasePagesAPIViewSet(BaseAPIViewSet):
    model = ReleasePage
    BaseAPIViewSet.known_query_parameters = BaseAPIViewSet.known_query_parameters.union([
        'release_types',
        'remove_listing',
    ])
    BaseAPIViewSet.listing_default_fields = BaseAPIViewSet.listing_default_fields + [
        'release_types',
    ]
    BaseAPIViewSet.filter_backends = BaseAPIViewSet.filter_backends + [
        ReleaseTypeFilter,
        RemoveListingFilter,
        SearchFilter,
    ]

class PsychicPagesAPIViewSet(BaseAPIViewSet):
    pass

# PagesAPIViewSet to only have PyschicPages and include categories numeric filter
class PsychicPagesAPIViewSet (PsychicPagesAPIViewSet):
    model = PsychicPage
    PsychicPagesAPIViewSet.known_query_parameters = PsychicPagesAPIViewSet.known_query_parameters.union([
        'type',
        'child_of',
        'ancestor_of',
        'descendant_of',
        'translation_of',
        'locale',
        'categories',
        'content_type__model'
    ])
    PsychicPagesAPIViewSet.filter_backends.append(CategoryFilter)

    def modify_queryset(self, queryset):
        '''We actually want PsychicPage Model instead of Page, but it is an abstract model.
            So, will filter to get only Page types that take Psychic page here '''
        
        filtered_ids = [x for x in queryset.values_list('id', flat=True)]
        page_types = ['videopage', 'artistpage', 'releasepage', 'journalpage',]
        full_queryset = None
        for page in page_types:
            partial_qs = getattr(queryset.model, page).related.related_model.objects.filter(pk__in=filtered_ids).values('id','title','category_image__file','url_path','content_type__model')
            if full_queryset:
                full_queryset = full_queryset.union(partial_qs)
            else:
                full_queryset = partial_qs

        return full_queryset 

    def get_queryset(self):
        return Page.objects.all().order_by('id')

    def listing_view(self, request):
        queryset = self.get_queryset()
        self.check_query_parameters(queryset)
        queryset = self.filter_queryset(queryset)
        queryset = self.paginate_queryset(queryset)
        modified_queryset = self.modify_queryset(queryset)
        serializer = self.get_serializer(modified_queryset, many=True)
        return self.get_paginated_response(serializer.data)

    def get_serializer_class(self, base=PsychicPagesSerializer):
        attrs = {
            'request': self.request,
        }
        return type('PsychicPagesSerializer', (base, ), attrs)
